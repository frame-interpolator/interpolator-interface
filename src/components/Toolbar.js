import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import PanToolIcon from '@material-ui/icons/PanTool';
import EditIcon from '@material-ui/icons/Edit';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import MouseIcon from '@material-ui/icons/Mouse';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';

const styles = {
    toolbarIcon: {
        color: 'rgba(0, 0, 0, 0.54)',
        display: 'flex',
    },
};

class ToolbarIcon extends React.Component {
    render() {
        return (
            <div style={styles.toolbarIcon}>
                <Tooltip title={this.props.tooltip} placement="right">
                    {this.props.children}
                </Tooltip>
            </div>
        );
    }
}

export default class Toolbar extends React.Component {
    state = {
        selectedIndex: 0,
    };

    componentDidMount() {
        document.addEventListener("keydown", this.handleKeyDown);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.handleKeyDown);
    }

    handleKeyDown = (event) => {
        if (this.props.disabled) {
            return;
        }
        const activeElementIsInput = document.activeElement instanceof HTMLInputElement;
        if (!!event.key && !activeElementIsInput) {
            if (event.key === '1' && !event.ctrlKey && !event.altKey) {
                this.setState({selectedIndex: 0});
                this.props.onModeChange(this.indexToMode(0));
            } else if (event.key === '2' && !event.ctrlKey && !event.altKey) {
                this.setState({selectedIndex: 1});
                this.props.onModeChange(this.indexToMode(1));
            } else if (event.key === '3' && !event.ctrlKey && !event.altKey) {
                this.setState({selectedIndex: 2});
                this.props.onModeChange(this.indexToMode(2));
            } else if (event.key === '4' && !event.ctrlKey && !event.altKey) {
                this.setState({selectedIndex: 3});
                this.props.onModeChange(this.indexToMode(3));
            } else if (event.key === '5' && !event.ctrlKey && !event.altKey) {
                this.setState({selectedIndex: 4});
                this.props.onModeChange(this.indexToMode(4));
            }
        }
    };

    handleListItemClick = (event, index) => {
        this.setState({selectedIndex: index});
        this.props.onModeChange(this.indexToMode(index));
    };

    getMode = () => {
        return this.indexToMode(this.state.selectedIndex);
    };

    indexToMode = (index) => {
        switch (index) {
        case 0:
            // Allows all behaviors at once.
            return 'default';
        case 1:
            // Allows only canvas panning.
            return 'pan';
        case 2:
            // Allows only adding new hints.
            return 'add';
        case 3:
            // Allows only removing hints.
            return 'remove';
        case 4:
            // Allows only moving hints.
            return 'move';
        }
    };

    render() {
        return (
            <div style={this.props.style}>
                <Paper>
                    <List component="nav">
                        <ListItem button disabled={this.props.disabled}
                            selected={this.state.selectedIndex === 0}
                            onClick={event => this.handleListItemClick(event, 0)}>
                            <ToolbarIcon tooltip="Default cursor (1)">
                                <MouseIcon fontSize="small"/>
                            </ToolbarIcon>
                        </ListItem>
                        <ListItem button disabled={this.props.disabled}
                            selected={this.state.selectedIndex === 1}
                            onClick={event => this.handleListItemClick(event, 1)}>
                            <ToolbarIcon tooltip="Pan (2)">
                                <PanToolIcon fontSize="small"/>
                            </ToolbarIcon>
                        </ListItem>
                        <ListItem button disabled={this.props.disabled}
                            selected={this.state.selectedIndex === 2}
                            onClick={event => this.handleListItemClick(event, 2)}>
                            <ToolbarIcon tooltip="Add hints (3)">
                                <AddCircleIcon fontSize="small"/>
                            </ToolbarIcon>
                        </ListItem>
                        <ListItem button disabled={this.props.disabled}
                            selected={this.state.selectedIndex === 3}
                            onClick={event => this.handleListItemClick(event, 3)}>
                            <ToolbarIcon tooltip="Remove hints (4)">
                                <RemoveCircleIcon fontSize="small"/>
                            </ToolbarIcon>
                        </ListItem>
                        <ListItem button disabled={this.props.disabled}
                            selected={this.state.selectedIndex === 4}
                            onClick={event => this.handleListItemClick(event, 4)}>
                            <ToolbarIcon tooltip="Move hints (5)">
                                <EditIcon fontSize="small"/>
                            </ToolbarIcon>
                        </ListItem>
                    </List>
                </Paper>
            </div>
        );
    }
}
