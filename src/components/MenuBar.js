import React from 'react';

// Button imports.
import Button from '@material-ui/core/Button';

import Divider from '@material-ui/core/Divider';

// Menu imports.
import Popover from '@material-ui/core/Popover';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';

// Dialogs.
import AboutDialog from './AboutDialog';
import VersionDialog from './VersionDialog';

// Paper imports.
import Paper from '@material-ui/core/Paper';

// Icons.
import UndoIcon from '@material-ui/icons/Undo';
import RedoIcon from '@material-ui/icons/Redo';

const styles = {
  paper : {
    textAlign: 'left',
    display: 'inline-block',
    width: '100%',
    backgroundColor: '#eeeeee',
  },
  menuBar : {
    width: '100%',
    // TODO: Make 37px a constant somewhere.
    height: '37px',
    zIndex: 5,
    position: 'relative'
  }
};

export default class MenuBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openFileMenu: false,
      openEditMenu: false,
      openViewMenu: false,
      openHelpMenu: false,
      numUndo: 0,
      numRedo: 0,
    };
  }

  componentDidMount() {
    if (!this.props.getActionStack().registerCountCallback(this.handleUndoRedoCountCallback)) {
      console.error('MenuBar could not unregister callback.');
    }
  }

  componentWillUnmount() {
    if (!this.props.getActionStack().unregisterCountCallback(this.handleUndoRedoCountCallback)) {
      console.error('MenuBar could not unregister callback.');
    }
  }

  handleUndoRedoCountCallback = (numUndo, numRedo) => {
    this.setState({
      numUndo: numUndo,
      numRedo: numRedo,
    })
  };

  handleFileClick = (event) => {
    // This prevents ghost click.
    event.preventDefault();
    this.setState({
      openFileMenu: true,
      anchorEl: event.currentTarget
    });
  };

  handleEditClick = (event) => {
    event.preventDefault();
    this.setState({
      openEditMenu: true,
      anchorEl: event.currentTarget
    });
  };

  handleViewClick = (event) => {
    event.preventDefault();
    this.setState({
      openViewMenu: true,
      anchorEl: event.currentTarget
    });
  };

  handleHelpClick = (event) => {
    event.preventDefault();
    this.setState({
      openHelpMenu: true,
      anchorEl: event.currentTarget
    });
  };

  handleRequestClose = () => {
    this.setState({
      openFileMenu: false,
      openEditMenu: false,
      openViewMenu: false,
      openHelpMenu: false,
    });
  };

  render() {
    return (
      <div id="menu-bar" style={styles.menuBar}>
        <Paper style={styles.paper} zdepth={2} square={true}>
          <Button onClick={this.handleFileClick}>
            File
          </Button>
          <Popover
            open={this.state.openFileMenu}
            anchorEl={this.state.anchorEl}
            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
            targetorigin={{horizontal: 'left', vertical: 'top'}}
            onClose={this.handleRequestClose}>
            <MenuList onClick={this.handleRequestClose}>
              <MenuItem onClick={this.props.loadImage1}>Load image 1&ensp;<sub>ctrl+alt+1</sub></MenuItem>
              <MenuItem onClick={this.props.loadImage2}>Load image 2&ensp;<sub>ctrl+alt+2</sub></MenuItem>
              <MenuItem onClick={this.props.saveHints}>Save hints&ensp;<sub>ctrl+alt+s</sub></MenuItem>
              <MenuItem onClick={this.props.loadHints}>Load hints&ensp;<sub>ctrl+alt+l</sub></MenuItem>
            </MenuList>
          </Popover>
          <Button onClick={this.handleEditClick}>
            Edit
          </Button>
          <Popover
            open={this.state.openEditMenu}
            anchorEl={this.state.anchorEl}
            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
            targetorigin={{horizontal: 'left', vertical: 'top'}}
            onClose={this.handleRequestClose}>
            <MenuList onClick={this.handleRequestClose}>
              <MenuItem onClick={this.props.undo} disabled={this.state.numUndo === 0}>
                <ListItemIcon><UndoIcon fontSize="small"/></ListItemIcon>
                Undo&ensp;<sub>ctrl+z</sub>
              </MenuItem>
              <MenuItem onClick={this.props.redo} disabled={this.state.numRedo === 0}>
                <ListItemIcon><RedoIcon fontSize="small"/></ListItemIcon>
                Redo&ensp;<sub>ctrl+y</sub>
              </MenuItem>
              <Divider/>
              <MenuItem onClick={this.props.deleteSelectedHint}>Delete selected hint&ensp;<sub>del</sub></MenuItem>
              <MenuItem onClick={this.props.clearHints}>Clear hints&ensp;<sub>ctrl+alt+c</sub></MenuItem>
              <Divider/>
              <MenuItem onClick={this.props.runInterpolator}>Run interpolator&ensp;<sub>ctrl+alt+r</sub></MenuItem>
              <MenuItem onClick={this.props.openInterpolatorSettings}>Interpolator settings</MenuItem>
            </MenuList>
          </Popover>
          <Button onClick={this.handleViewClick}>
            View
          </Button>
          <Popover
            open={this.state.openViewMenu}
            anchorEl={this.state.anchorEl}
            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
            targetorigin={{horizontal: 'left', vertical: 'top'}}
            onClose={this.handleRequestClose}>
            <MenuList onClick={this.handleRequestClose}>
              <MenuItem onClick={this.props.toggleAdvancedView}>Advanced view&ensp;<sub>a</sub></MenuItem>
            </MenuList>
          </Popover>
          <Button onClick={this.handleHelpClick}>
            Help
          </Button>
          <Popover
            open={this.state.openHelpMenu}
            anchorEl={this.state.anchorEl}
            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
            targetorigin={{horizontal: 'left', vertical: 'top'}}
            onClose={this.handleRequestClose}>
            <MenuList onClick={this.handleRequestClose}>
              <MenuItem
                onClick={() => {if (this.refs.versionDialog) this.refs.versionDialog.open()}}>
                Version
              </MenuItem>
              <MenuItem
                onClick={() => {if (this.refs.aboutDialog) this.refs.aboutDialog.open()}}>
                About
              </MenuItem>
            </MenuList>
          </Popover>
        </Paper>
        <VersionDialog ref='versionDialog'/>
        <AboutDialog ref='aboutDialog'/>
      </div>
    );
  }
}
