import React from 'react';
import PropTypes from 'prop-types';
import { Divider } from '@material-ui/core';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';

import EasyGL from '../utils/EasyGL';

const styles ={
    root: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    tabContainer: {
        overflow: 'hidden',
        flex: 1,
        height: '100%',
        width: '100%',
        display: 'flex',
        padding: 0,
    },
};

function TabContainer({ children }) {
    return (
        <Typography component="div" style={styles.tabContainer}>
            {children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired
};

export default class AdvancedView extends React.Component {
    constructor(props) {
        super(props);

        // Set initial state.
        this.state = {
            value: 0,
        };

        // Tracks changes to the canvas reference.
        this.prevCanvasRef = null;
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleResize);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize);
    }

    componentDidUpdate() {
        this.redraw();
    }

    handleResize = () => {
        if (!this.easyGL) {
            return;
        }
        this.easyGL.resize_canvas();
        this.forceUpdate();
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    handleChangeIndex = index => {
        this.setState({ value: index });
    };

    redraw = () => {
        if (!this.easyGL || !this.img) {
            return;
        }
        this.easyGL.clearscreen();
        if (!!this.refs.image && !!this.refs.image.src && !!this.img.src) {
            try {
                this.easyGL.drawimage(this.img, 0, 0);
            } catch (error) {
                // Do nothing.
            }
        }
    };

    onLoadImage = () => {
        if (!this.img) {
            this.img = null;
            this.easyGL = null;
        }
        if (this.prevCanvasRef !== this.refs.canvas || !this.easyGL) {
            // Does not create a new easygl instance unless the canvas ref has changed.
            this.easyGL = new EasyGL(this.refs.canvas.getContext('2d'));
            this.prevCanvasRef = this.refs.canvas;
        }
        this.img = this.refs.image;
        this.handleResize();
    };

    /*************************************************************************
     * Mouse Events                                                          *
     *************************************************************************/
    eventToCoord = (event) => {
        let coords = null;
        if (!!event.touches) {
            if (event.touches.length === 0) {
                return null;
            }
            coords = this.pageToCanvasCoord(event.touches[0].clientX, event.touches[0].clientY);
        } else {
            coords = this.pageToCanvasCoord(event.clientX, event.clientY);
        }
        return coords;
    };

    handleMouseDown = (event) => {
        if (!this.imageLoaded()) {
            return;
        }
        
        let coords = this.eventToCoord(event);
        if (!coords) {
            return;
        }
        const [x, y] = coords;
        this.panning = {
            prevx: x,
            prevy: y
        };
    };

    handleMouseUp = (event) => {
        if (!this.imageLoaded()) {
            return;
        }

        if (this.panning) {
            delete this.panning;
        }
    };

    handleMouseMove = (event) => {
        if (!this.imageLoaded()) {
            return;
        }

        if (this.panning) {
            // Do everything relative to original canvas.
            let coords = this.eventToCoord(event);
            if (!coords) {
                return;
            }
            const [x, y] = coords;
            const dx = x - this.panning.prevx;
            const dy = y - this.panning.prevy;
            this.panning.prevx = x;
            this.panning.prevy = y;
            this.easyGL.translate_screen(dx, dy);
            this.redraw();
        }
    };

    handleScroll = (event) => {
        if (!this.imageLoaded()) {
            return;
        }

        let coords = this.eventToCoord(event);
        if (!coords) {
            return;
        }
        const [x, y] = coords;
        if (event.deltaY < 0) {
            this.easyGL.zoom_in_to_screen(x, y);
        } else if (event.deltaY > 0) {
            this.easyGL.zoom_out_at_screen(x, y);
        } else {
            return;
        }
        this.redraw();
    };

    /*************************************************************************
     * Event Utilities                                                       *
     *************************************************************************/
    pageToCanvasCoord = (x, y) => {
        const rect = this.refs.canvas.getBoundingClientRect();
        return [x - rect.left, y - rect.top];
    };

    imageLoaded = () => {
        return !!this.easyGL && !!this.img;
    };

    getHasImages = () => {
        return !!this.props.advancedState && !!this.props.advancedState.images &&
            this.props.advancedState.images.length > 0
    };

    getUpdatedIndex = () => {
        let newIndex = this.state.value;
        if (this.getHasImages()) {
            const length = this.props.advancedState.images.length;
            if (newIndex >= length) {
                newIndex = length - 1;
            }
        } else {
            newIndex = 0;
        }
        // Silent update.
        this.state.value = newIndex;
        return newIndex;
    };

    getTabs = () => {
        if (this.getHasImages()) {
            return this.props.advancedState.images.map((image) => {
                return <Tab label={image.name} key={image.name}/>;
            });
        }
        if (!this.props.advancedState) {
            return [<Tab label="Interpolator hasn't been run yet." key="empty"/>];
        }
        return [<Tab label="Interpolator didn't return any advanced images." key="empty"/>];
    };

    getTabContainer = () => {
        if (this.getHasImages()) {
            const images = this.props.advancedState.images;
            const i = this.getUpdatedIndex();
            return (
                <div style={{overflow: 'hidden', flex: 1, height: '100%', width: '100%', display: 'flex'}}>
                    <TabContainer key="imageViewer">
                        <canvas ref="canvas"
                            onTouchStart={(e) => this.handleMouseDown(e.nativeEvent)}
                            onTouchEnd={(e) => this.handleMouseUp(e.nativeEvent)}
                            onTouchMove={(e) => this.handleMouseMove(e.nativeEvent)}
                            onMouseDown={(e) => this.handleMouseDown(e)}
                            onMouseUp={(e) => this.handleMouseUp(e)}
                            onMouseMove={(e) => this.handleMouseMove(e)}
                            onWheel={(e) => this.handleScroll(e)}
                            style={{height: '100%', width: '100%'}}
                        />
                    </TabContainer>
                    <img ref="image" src={images[i].src}
                        style={{display: 'none'}}
                        onLoad={this.onLoadImage}/>
                </div>
            );
        }
        return [<TabContainer key="empty"> </TabContainer>]
    };

    render() {
        var ghetto = 0;
        return (
            <div style={styles.root}>
                {/* <AppBar position="static" color="default" style={{flex: '0'}}> */}
                <div style={{flex: 0, position: 'static', background: 'white'}}>
                    <Tabs
                        value={this.getUpdatedIndex()}
                        onChange={this.handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant={this.getHasImages() ? 'scrollable' : 'fullWidth'}
                        scrollButtons="auto">
                        {this.getTabs()}
                    </Tabs>
                </div>
                <Divider/>
                {/* </AppBar> */}
                {this.getTabContainer()}
            </div>
        );
    }
}
