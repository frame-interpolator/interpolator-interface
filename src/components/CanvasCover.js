import React from 'react';

import Typography from '@material-ui/core/Typography';

const styles = {
    canvasCover: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        pointerEvents: 'none',
        zIndex: 3,
        flexDirection: 'column',
        justifyContent: 'center',
        background: 'rgba(0, 0, 0, 0.03)',
        display: 'flex',
        alignItems: 'center',
        transition: 'opacity 500ms ease',
        opacity: 0,
      },
      canvasCoverContents: {
        flex: '0 0 auto',
        textAlign: 'center',
        margin: '0 auto',
        display: 'block',
      },
};

// Covers the canvas until a condition is fulfilled.
export default class CanvasCover extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let canvasCover = Object.assign({}, styles.canvasCover);
        if (this.props.show) {
            canvasCover.opacity = 1;
            canvasCover.pointerEvents = 'all';
        }
        return (
            <div style={canvasCover}>
                <div style={styles.canvasCoverContents}>
                    {this.props.children}
                </div>
                <Typography style={styles.canvasCoverContents}>
                    {this.props.message}
                </Typography>
            </div>
        );
    }
  }
