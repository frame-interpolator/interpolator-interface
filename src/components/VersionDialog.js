import React from 'react';
import Typography from '@material-ui/core/Typography';

import BasicDialog from './BasicDialog';

export default class VersionDialog extends React.Component {
  open = () => {
    this.refs.dialog.open();
  }

  render() {
    return (
      <BasicDialog ref='dialog' title="Version" closeText="Close">
        <Typography gutterBottom>
          2D Animation Inbetweener
        </Typography>
        <Typography gutterBottom>
          <b>0.0.1r0 alpha</b>
        </Typography>
      </BasicDialog>
    );
  }
}
