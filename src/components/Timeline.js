import React from "react";
import BezierCanvas from "./BezierCanvas";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import MultiSlider from "./MultiSlider";
import BezierEasing from 'bezier-easing';

const styles = {
    timelineContainer: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    verticalDivider: {
        borderLeft: '1px solid rgb(220, 220, 220)',
        width: '1px',
        height: '100%',
        flex:'0',
    },
    curveContainer: {
        display: 'flex',
        flexDirection: 'column',
        flex: '1',
        height: '100%',
        minHeight: '0px',
        alignItems: 'center',
        minWidth: '350px',
    },
    numFramesContainer: {
        display: 'flex',
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: '10px',
        paddingLeft: '20px',
        paddingRight: '20px',
    },
};

const actionCtxName = 'Timeline';

export default class Timeline extends React.Component {
    constructor(props) {
        super(props);

        // Set initial state.
        this.state = {
            bezierValues: [0.25, 0.25, 0.75, 0.75],
            sliderValues: [],
            numFrames: 5,
        };
    }

    componentDidMount() {
        this._updateSliderFromBezier();
        if (!this.props.getActionStack().registerUndoRedoCallback(actionCtxName,
            this.handleUndoRedoCallback, this.state)) {
            console.error('Timeline could not register callback.');
        }
    }

    componentWillUnmount() {
        if (!this.props.getActionStack().unregisterUndoRedoCallback(actionCtxName)) {
            console.error('Timeline could not unregister callback.');
        }
    }

    handleUndoRedoCallback = (currentState, context) => {
        if (context === actionCtxName && currentState !== null) {
            this.setState(currentState, () => {
                this.refs.bezierCanvas.setValues(this.state.bezierValues);
                this._updateSliderFromBezier();
            });
        }
    };

    addAction = () => {
        this.props.getActionStack().addState(this.state, actionCtxName);
    };

    getTimeSlider = () => {
        return this.refs.tSlider;
    };

    bezierCanvasResized = () => {
        if (this.refs.bezierCanvas) {
            this.refs.bezierCanvas.handleResize();
        }
    };

    /**
     * Called during manual modification to the curve.
     * @param value
     */
    onCurveChange = (value) => {
        this.setState({bezierValues: value}, () => {
            this._updateSliderFromBezier();
        });
    };

    /**
     * Called when manual modification to the curve is finished.
     * @param value
     */
    onCurveChanged = (value) => {
        this.setState({bezierValues: value}, () => {
            this._updateSliderFromBezier();
            this.addAction();
        });
    };

    /**
     * Called during manual modifications to the slider.
     * @param values
     */
    onSliderChange = (values) => {
        this.setState({sliderValues: values});
    };

    /**
     * Called when the manual modification has finished.
     * @param values
     */
    onSliderChanged = (values) => {
        this.setState({sliderValues: values}, this.addAction);
    };

    /**
     * Called when the number of frames is changed.
     * @param event
     */
    onNumFramesChange = (event) => {
        const numFrames = parseInt(event.target.value);
        this.setState({numFrames: numFrames}, () => {
            this._updateSliderFromBezier();
            this.addAction();
        });
    };

    /**
     * Updates the slider values from the bezier if there are bezier values and num frames.
     * If there are no frames, then the slider values will be explicitly set to whatever the current values are.
     * @private
     */
    _updateSliderFromBezier = () => {
        // Calculate timeline values based on bezier curve.
        let easing = BezierEasing(...this.state.bezierValues);
        let sliderValues = [];
        if (!isNaN(this.state.numFrames)) {
          for (let i = 1; i <= this.state.numFrames; ++i) {
            const x = i / (this.state.numFrames + 1);
            sliderValues.push(easing(x));
          }
        }
        if (sliderValues.length !== 0) {
            this.state.sliderValues = sliderValues;
            this.refs.tSlider.setValues(sliderValues);
        } else {
            this.refs.tSlider.setValues(this.state.sliderValues);
        }
    };

    render() {
        return (
            <div style={styles.timelineContainer}>
                <div style={styles.curveContainer}>
                    <BezierCanvas show={this.state.numFrames === 0} ref="bezierCanvas"
                                  padding={0} onChange={this.onCurveChange} onChanged={this.onCurveChanged}
                                  style={{height: '100%', width: '100%', display: 'flex',
                        paddingTop: '10px', paddingBottom: '5px'}}/>
                    <div style={styles.numFramesContainer}>
                        <Typography inline={true}
                            style={{paddingRight: '10px'}}>
                            Number of bezier frames:
                        </Typography>
                        <TextField value={isNaN(this.state.numFrames) ? '' : this.state.numFrames}
                            onChange={this.onNumFramesChange}
                            type="number" style={{width: '100%', flex: 1}} />
                    </div>
                </div>
                <div style={styles.verticalDivider}/>
                <MultiSlider ref='tSlider' onChange={this.onSliderChange} onChanged={this.onSliderChanged}
                             style={{flex: '3'}} locked={this.state.numFrames !== 0}/>
            </div>
        );
    }
}