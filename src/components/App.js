import React from 'react';

import {
  ReflexContainer,
  ReflexSplitter,
  ReflexElement
} from 'react-reflex';

// Custom component imports.
import MenuBar from './MenuBar';
import MainHintWindow from './MainHintWindow';
import MultiSlider from './MultiSlider';
import HintView from './HintView';
import ImageViewer from './ImageViewer';
import AdvancedView from './AdvancedView';
import CustomizedSnackbars from './SnackBar';
import Timeline from './Timeline';
import ActionStack from '../utils/ActionStack';

// Theme imports.
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import orange from '@material-ui/core/colors';

// Material imports.
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

// CSS imports.
import './reflexstyle.css';

// Global theme.
const muiTheme = createMuiTheme({
  palette: {
    accent1Color: orange,
  },
  typography: {
    useNextVariants: true,
  },
});

const styles = {
  layout: {
    // TODO: Make 37px (the menu bar height) a constant somewhere.
    height: 'calc(100% - 37px)',
    width: '100%',
    position: 'relative',
    opacity: 0,
    transition: 'opacity 600ms ease',
    userSelect: 'none',
    MozUserSelect: 'none',
  },
  titleContainer: {
    container: {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      height: '100%',
    },
    title: {
      position: 'relative',
      zIndex: 2,
    },
    content: {
      position: 'relative',
      zIndex: 1,
      flex: 1,
      display: 'flex',
      overflow: 'hidden',
    },
  },
  timelineContainer: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  verticalDivider: {
    borderLeft: '1px solid rgb(220, 220, 220)',
    width: '1px',
    height: '100%',
    flex:'0',
  },
  curveContainer: {
    display: 'flex',
    flexDirection: 'column',
    flex: '1',
    height: '100%',
    minHeight: '0px',
    alignItems: 'center',
    minWidth: '350px',
  },
  numFramesContainer: {
    display: 'flex',
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: '10px',
    paddingLeft: '20px',
    paddingRight: '20px',
  },
};

class TitledContainer extends React.Component {
  createTitleBar = (title) => {
    return (
      <AppBar position="static">
        <Toolbar variant="dense">
          <Typography variant="subtitle1" color="inherit">
            {title}
          </Typography>
        </Toolbar>
      </AppBar>);
  };

  render () {
    return (
      <div style={styles.titleContainer.container}>
        <div style={styles.titleContainer.title}>
          {this.createTitleBar(this.props.title)}
        </div>
        <div style={styles.titleContainer.content}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default class App extends React.Component {
  constructor(props) {
    super(props);

    // Set initial state.
    this.state = {
      currentView: 'main',
      showAdvancedView: false,
    };

    /**
     * @type {ActionStack}
     */
    this.actionStack = new ActionStack();
  }

  componentDidMount() {
    // Animate the opacity on load.
    this.refs.mainBody.style.opacity = 1;
    document.addEventListener("keydown", this.handleKeyDown);
    document.onpaste = this.handlePaste;
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyDown);
  }

  handlePaste = (event) => {
    if (this.refs.hintView) {
      this.refs.hintView.handlePaste(event);
    }
  };

  handleKeyDown = (event) => {
    if (!!event.key) {
      if (event.key === 'a') {
        this.toggleAdvancedView();
      } else if (event.key === 's' && event.ctrlKey && event.altKey) {
        this.refs.hintView.saveHint();
      } else if (event.key === 'l' && event.ctrlKey && event.altKey) {
        this.refs.hintView.loadHint();
      } else if (event.key === '1' && event.ctrlKey && event.altKey) {
        this.refs.hintView.loadImage1();
      } else if (event.key === '2' && event.ctrlKey && event.altKey) {
        this.refs.hintView.loadImage2();
      } else if (event.key === 'c' && event.ctrlKey && event.altKey) {
        this.refs.hintView.handleClearHints();
      } else if (event.key === 'r' && event.ctrlKey && event.altKey) {
        this.refs.hintView.handleRunInterpolator();
      } else if (event.key === 'z' && event.ctrlKey) {
        event.preventDefault();
        this.handleUndo();
      } else if (event.key === 'y' && event.ctrlKey) {
        event.preventDefault();
        this.handleRedo();
      }
    }
  };

  toggleAdvancedView = () => {
    this.setState(Object.assign({}, this.state,
      {showAdvancedView: !this.state.showAdvancedView}));
    // Resize the image viewer on the next rendered frame so that the height it gets
    // is up to date.
    window.requestAnimationFrame(() => {
      this.imageViewerResized();
    });
  };

  cbChangeMainView = (view) => {
    this.setState(Object.assign({}, this.state, {currentView: view}));
  };

  getTimeSlider = () => {
    return this.refs.timeline.getTimeSlider();
  };

  getViewer = () => {
    return this.refs.imageViewer;
  };

  setAdvancedViewState = (state) => {
    this.setState(Object.assign({}, this.state, {advancedViewState: state}));
  };

  hintViewResized = () => {
    if (this.refs.hintView) {
      this.refs.hintView.handleResize();
      this.imageViewerResized();
    }
  };

  imageViewerResized = () => {
    if (this.refs.imageViewer) {
      this.refs.imageViewer.handleResize();
      this.advancedViewerResized();
    }
  };

  advancedViewerResized = () => {
    if (this.refs.advancedViewer) {
      this.refs.advancedViewer.handleResize();
    }
  };

  bezierCanvasResized = () => {
    if (this.refs.timeline) {
      this.refs.timeline.bezierCanvasResized();
    }
  };

  everythingResized = () => {
    this.hintViewResized();
  };

  getSnackBar = () => {
    return this.refs.snackbar;
  };

  getActionStack = () => {
    return this.actionStack;
  };

  handleUndo = () => {
    this.actionStack.undo();
  };

  handleRedo = () => {
    this.actionStack.redo();
  };

  getMainComponent() {
    let mainComponent;
    switch(this.state.currentView) {
      case 'hint_old':
        mainComponent = (
          <MainHintWindow id='hint-window'
            cbChangeMainView={this.cbChangeMainView} />
        );
        break;
      case 'main':
        mainComponent = (
          <ReflexContainer orientation="horizontal">
            <ReflexElement onResize={this.everythingResized} propagateDimensions={true}>
              <ReflexContainer orientation="vertical">
                <ReflexElement onResize={this.hintViewResized} propagateDimensions={true}>
                  <TitledContainer title={'Images and Hints'}>
                    <HintView id='hint-view' ref='hintView'
                      getTimeSlider={this.getTimeSlider}
                      getViewer={this.getViewer}
                      setAdvancedViewState={this.setAdvancedViewState}
                      getSnackBar={this.getSnackBar}
                      getActionStack={this.getActionStack}/>
                  </TitledContainer>
                </ReflexElement>

                <ReflexSplitter propagate={true}/>

                <ReflexElement
                  propagateDimensions={true} size="700">
                  <ReflexContainer orientation="horizontal">
                    <ReflexElement onResize={this.imageViewerResized}
                      propagateDimensions={true}>
                      <TitledContainer title={'Viewer'}>
                        <ImageViewer id='image-viewer' ref='imageViewer'
                          getSnackBar={this.getSnackBar}/>
                      </TitledContainer>
                    </ReflexElement>

                    {this.state.showAdvancedView && <ReflexSplitter propagate={true}/>}
                    {
                      this.state.showAdvancedView && 
                      <ReflexElement size="400"
                        propagateDimensions={true}>
                        <TitledContainer title={'Advanced Viewer'}>
                          <AdvancedView ref="advancedViewer"
                            advancedState={this.state.advancedViewState}/>
                        </TitledContainer>
                      </ReflexElement>
                    }
                  </ReflexContainer>
                </ReflexElement>
              </ReflexContainer>
            </ReflexElement> 

            <ReflexSplitter propagate={true}/>
            
            <ReflexElement propagateDimensions={true} resizeHeight={true} size={200}
              onResize={this.bezierCanvasResized}>
              <TitledContainer title={'Timeline'}>
                <Timeline ref="timeline"
                  getActionStack={this.getActionStack}/>
              </TitledContainer>
            </ReflexElement>
          </ReflexContainer>
        );
        break;
      default:
        alert("Invalid view: " + this.state.currentView);
        break;
    }
    return mainComponent;
  }

  render () {
    return (
      <MuiThemeProvider theme={muiTheme}>
        <MenuBar cbChangeMainView={this.cbChangeMainView}
          loadImage1={() => {if (this.refs.hintView) this.refs.hintView.loadImage1();}}
          loadImage2={() => {if (this.refs.hintView) this.refs.hintView.loadImage2();}}
          loadHints={() => {if (this.refs.hintView) this.refs.hintView.loadHint();}}
          saveHints={() => {if (this.refs.hintView) this.refs.hintView.saveHint();}}
          clearHints={() => {if (this.refs.hintView) this.refs.hintView.handleClearHints();}}
          openInterpolatorSettings={() =>{if (this.refs.hintView) this.refs.hintView.openInterpolatorSettings();}}
          deleteSelectedHint={() => {if (this.refs.hintView) this.refs.hintView.deleteSelectedHint();}}
          runInterpolator={() => {if (this.refs.hintView) this.refs.hintView.handleRunInterpolator();}}
          toggleAdvancedView={this.toggleAdvancedView}
          undo={this.handleUndo}
          redo={this.handleRedo}
          getActionStack={this.getActionStack}/>
        <div id="layout" style={styles.layout} ref='mainBody'>
            {this.getMainComponent()}
        </div>
        <CustomizedSnackbars ref='snackbar'/>
      </MuiThemeProvider>
    )
  }
}
