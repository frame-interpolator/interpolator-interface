import React from 'react';

import CanvasCover from './CanvasCover';
import EasyGL from '../utils/EasyGL';

const styles = {
  canvasContainer: {
    display: 'flex',
    position: 'relative',
    height: '100%',
    width: '100%',
  },
  canvas: {
    position: 'relative',
    zIndex: 1,
    outline: 'none',
    height: '100%',
    width: '100%',
  }
};

export default class BezierCanvas extends React.Component {
  constructor(props) {
    super(props);

    this.padLeft = props.padding || props.padWidth || props.padLeft || 0;
    this.padRight = props.padding || props.padWidth || props.padRight || 0;
    this.padTop = props.padding || props.padHeight || props.padTop || 0;
    this.padBottom = props.padding || props.padHeight || props.padBottom || 0;
    this.radius = props.radius || 5;
    let values = props.values || [0.25, 0.25, 0.75, 0.75];

    // Set initial state.
    this.state = {
      values: values
    };
  }

  componentDidMount() {
    this.easyGL = new EasyGL(this.refs.bcanvas.getContext('2d'));
    this.handleResize();
    window.addEventListener("resize", this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
  }

  componentDidUpdate() {
    this.redraw();
  }

  static getDerivedStateFromProps(props, state) {
    if (props.values && props.values.length !== 0) {
      return {
        values: props.values
      };
    }
    return {};
  }

  /**
   * Overrides the values with the given values.
   * @param values
   */
  setValues = (values) => {
    this.setState({values: values}, this.redraw);
  };

  handleResize = () => {
    this.easyGL.resize_canvas();
    this.redraw();
  };

  redraw = () => {
    if (!this.easyGL) {
      return;
    }
    this.easyGL.clearscreen();
    const height = this.refs.bcanvas.height - this.padTop - this.padBottom;
    const width = this.refs.bcanvas.width - this.padLeft - this.padRight;

    const xstart = this.padLeft;
    const ystart = this.padTop + height;
    const xend = this.padLeft + width;
    const yend = this.padTop;
    const [x1, y1, x2, y2] = this.valuesToCanvasCoord(this.state.values);
    this.easyGL.setFillStyle('#888888');
    this.easyGL.setStrokeStyle('#888888');
    this.easyGL.drawbezier(xstart, ystart, x1, y1, x2, y2, xend, yend);
    this.easyGL.setFillStyle('#3f51b5');
    this.easyGL.setStrokeStyle('#3f51b5');
    this.easyGL.ctx.setLineDash([10, 5]);
    this.easyGL.drawline(xstart, ystart, x1, y1);
    this.easyGL.drawline(xend, yend, x2, y2);
    this.easyGL.ctx.setLineDash([]);
    this.easyGL.fillcircle(x1, y1, this.radius);
    this.easyGL.fillcircle(x2, y2, this.radius);
    this.easyGL.drawcircle(x1, y1, this.radius + 2);
    this.easyGL.drawcircle(x2, y2, this.radius + 2);
  };

  /***************************************************************************
   * Event Utilities                                                         *
   ***************************************************************************/
  pageToCanvasCoord = (x, y) => {
    const rect = this.refs.bcanvas.getBoundingClientRect();
    return [x - rect.left, y - rect.top];
  };

  valuesToCanvasCoord = (values) => {
    const height = this.refs.bcanvas.height - this.padTop - this.padBottom;
    const width = this.refs.bcanvas.width - this.padLeft - this.padRight;
    return [this.padLeft + values[0] * width,
            this.padTop + height - values[1] * height,
            this.padLeft + values[2] * width,
            this.padTop + height - values[3] * height]
  };

  canvasCoordToValues = (x, y) => {
    const height = this.refs.bcanvas.height - this.padTop - this.padBottom;
    const width = this.refs.bcanvas.width - this.padLeft - this.padRight;
    return [(x - this.padLeft) / width, 1 - (y - this.padTop) / height]
  };

  /***************************************************************************
   * Mouse Events                                                            *
   ***************************************************************************/
  eventToCoord = (event) => {
    let coords = null;
    if (!!event.touches) {
      if (event.touches.length === 0) {
        return null;
      }
      coords = this.pageToCanvasCoord(event.touches[0].clientX, event.touches[0].clientY);
    } else {
      coords = this.pageToCanvasCoord(event.clientX, event.clientY);
    }
    return coords;
  };

  handleMouseDown = (event) => {
    let coords = this.eventToCoord(event);
    if (!coords) {
      return;
    }
    const [x, y] = coords;
    const [x1, y1, x2, y2] = this.valuesToCanvasCoord(this.state.values);
    if ((x-x1)*(x-x1) + (y-y1)*(y-y1) < 4 * this.radius * this.radius) {
      this.dragging = {
        value: 0,
      };
    } else if ((x-x2)*(x-x2) + (y-y2)*(y-y2) < 4 * this.radius * this.radius) {
      this.dragging = {
        value: 1,
      }
    }
  };

  _getValues = () => {
    return this.state.values.map(x => Math.max(0, Math.min(1, x)));
  };

  handleMouseUp = (event) => {
    if (this.dragging) {
      this.onChanged();
      delete this.dragging;
    }
  };

  handleMouseMove = (event) => {
    if (this.dragging) {
      let coords = this.eventToCoord(event);
      if (!coords) {
        return;
      }
      let [x, y] = coords;
      [x, y] = this.canvasCoordToValues(x, y);
      this.state.values[this.dragging.value * 2] = x;
      this.state.values[this.dragging.value * 2 + 1] = y;
      this.onChange();
      this.redraw();
    }
  };

  /**
   * Called while manually changing.
   */
  onChange = () => {
    if (this.props.onChange) {
      this.props.onChange(this._getValues());
    }
  };

  /**
   * Called once the manual change has completed.
   */
  onChanged = () => {
    this.onChange();
    if (!!this.props.onChanged) {
      this.props.onChanged(this._getValues());
    }
  };

  render() {
    return (
      <div id='bezier-view' style={this.props.style}>
        <div style={styles.canvasContainer}>
          <canvas ref='bcanvas'
            onTouchStart={(e) => {this.handleMouseDown(e.nativeEvent)}}
            onTouchEnd={(e) => {this.handleMouseUp(e.nativeEvent)}}
            onTouchMove={(e) => {this.handleMouseMove(e.nativeEvent)}}
            onMouseDown={this.handleMouseDown}
            onMouseUp={this.handleMouseUp}
            onMouseMove={this.handleMouseMove}
            style={styles.canvas}
          />
          <CanvasCover show={this.props.show}
            message='No bezier frames. Timeline can be edited directly.'/>
        </div>
      </div>
    );
  }
}
