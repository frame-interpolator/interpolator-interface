import React from 'react';

import IconButton from '@material-ui/core/IconButton';
import Slider from '@material-ui/lab/Slider';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Tooltip from '@material-ui/core/Tooltip';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import CloudDownload from '@material-ui/icons/CloudDownload';
import JSZip from 'jszip';
import saveAs from 'file-saver';

import CanvasCover from './CanvasCover';

import EasyGL from '../utils/EasyGL';
import { Divider } from '@material-ui/core';

const styles = {
  layout: {
    height: '100%',
    width: '100%',
    display: 'inline-flex',
    flexDirection: 'column',
  },
  canvasContainer: {
    flex: '1 1 0%',
    minHeight: '0',
    position: 'relative',
    zIndex: 1,
    display: 'flex',
  },
  canvas: {
    flex: '1 1 0%',
    height: '100%',
    width: '100%',
    position: 'relative',
    zIndex: 1,
    outline: 'none',
  },
  sliderContainer: {
    flex: '1',
    padding: '0px 10px',
    display: 'flex',
    alignItems: 'center',
  },
  slider: {
    // For some reason, material-ui/lab/Slider is very gay. The slider doesn't
    // support any padding on the element, so the slider won't follow the cursor
    // and won't decrease in width.
    padding: '25px 0px',
    width: '100%',
  },
  gridPaper: {
    height: '100%'
  },
  controlsContainer: {
    flex: '0 1 auto',
    position: 'relative',
    zIndex: 2,
    background: 'white',
    outline: 'none',
  },
  textContainer: {
    padding: '10px',
    display: 'flex',
    alignItems: 'center',
  },
  loading: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    pointerEvents: 'none',
    zIndex: 3,
    background: 'rgba(0, 0, 0, 0.35)',
    display: 'flex',
    alignItems: 'center',
    transition: 'opacity 500ms ease',
    opacity: 0,
  },
  loadingSpinner: {
    margin: '0 auto',
  },
};

export default class ImageViewer extends React.Component {
  constructor(props) {
    super(props);

    // Set initial state.
    this.state = {
      slider: 0,
      srcs: [],
      isPlaying: false,
      isLoading: false,
    };

    this.timer = null;
  }

  startLoading = () => {
    this.setState(Object.assign({}, this.state, {isLoading: true}));
  };

  stopLoading = () => {
    this.setState(Object.assign({}, this.state, {isLoading: false}));
  };

  setSrcs = (srcs) => {
    let newState = Object.assign({}, this.state, {srcs: srcs});
    this.setState(newState);
  };

  /***************************************************************************
   * Lifecycle functions                                                     *
   ***************************************************************************/
  componentDidMount() {
    this.easyGL = new EasyGL(this.refs.icanvas.getContext('2d'));
    window.addEventListener("resize", this.handleResize);
    this.handleResize();
    const active = this.getActiveImg();
    this.refs.img.src = this.state.srcs[active];
    document.addEventListener("keydown", this.handleKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
    document.removeEventListener("keydown", this.handleKeyDown);
  }

  componentDidUpdate() {
    const active = this.getActiveImg();
    this.refs.img.src = this.state.srcs[active];
  }

  handleResize = () => {
    this.easyGL.resize_canvas();
    this.redraw();
  };

  handleKeyDown = (event) => {
    const activeEl = document.activeElement;
    if (activeEl === this.refs.icanvas && !!event.key) {
      if (event.key === ' ') {
        this.handlePlay();
      } else if (event.key === 'ArrowRight') {
        let newValue = this.state.slider + 0.1;
        if (newValue > 1) {
          newValue = 1;
        }
        if (newValue !== this.state.slider) {
          this.setState(Object.assign({}, this.state, {slider: newValue}));
        }
      } else if (event.key === 'ArrowLeft') {
        let newValue = this.state.slider - 0.1;
        if (newValue < 0) {
          newValue = 0;
        }
        if (newValue !== this.state.slider) {
          this.setState(Object.assign({}, this.state, {slider: newValue}));
        }
      }
    }
  };

  redraw = () => {
    if (!this.easyGL || !this.state.srcs) {
      return;
    }

    this.easyGL.clearscreen();
    try {
      this.easyGL.drawimage(this.refs.img, 0, 0);
    } catch (error) {
      // Do nothing.
    }
  };

  /***************************************************************************
   * Event Utilities                                                         *
   ***************************************************************************/
  getActiveImg = () => {
    const numSrcs = this.state.srcs.length;
    const active = Math.floor(this.state.slider * numSrcs);
    if (active >= numSrcs) {
      return numSrcs - 1;
    }
    return active;
  };

  pageToCanvasCoord = (x, y) => {
    const rect = this.refs.icanvas.getBoundingClientRect();
    return [(x - rect.left), (y - rect.top)];
  };

  /***************************************************************************
   * Mouse Events                                                            *
   ***************************************************************************/
  eventToCoord = (event) => {
    let coords = null;
    if (!!event.touches) {
      if (event.touches.length === 0) {
        return null;
      }
      coords = this.pageToCanvasCoord(event.touches[0].clientX, event.touches[0].clientY);
    } else {
      coords = this.pageToCanvasCoord(event.clientX, event.clientY);
    }
    return coords;
  };

  handleMouseDown = (event) => {
    let coords = this.eventToCoord(event);
    if (!coords) {
      return;
    }
    const [x, y] = coords;

    this.panning = {
      prevx: x,
      prevy: y,
    }
  };

  handleMouseUp = (event) => {
    if (this.panning) {
      delete this.panning;
    }
  };

  handleMouseMove = (event) => {
    if (this.panning) {
      let coords = this.eventToCoord(event);
      if (!coords) {
        return;
      }
      const [x, y] = coords;
      const dx = x - this.panning.prevx;
      const dy = y - this.panning.prevy;
      this.panning.prevx = x;
      this.panning.prevy = y;
      this.easyGL.translate_screen(dx, dy);
      this.redraw();
    }
  };

  handleScroll = (event) => {
    const [x, y] = this.pageToCanvasCoord(event.clientX, event.clientY);
    if (event.deltaY < 0) {
      // Scrolling up.
      this.easyGL.zoom_in_to_screen(x, y);
    } else if (event.deltaY > 0) {
      // Scrolling down.
      this.easyGL.zoom_out_at_screen(x, y);
    } else {
      return;
    }
    this.redraw();
  };

  handleDoubleClick = (event) => {
    // Do nothing.
  };

  handleSlider = (event, value) => {
    this.setState(Object.assign({}, this.state, {slider: value / 100}));
  };

  /***************************************************************************
   * Button Events                                                           *
   ***************************************************************************/
  handlePlay = (event) => {
    if (!this.timer) {
      // Start playing.
      let newState = Object.assign({}, this.state, {isPlaying: true});
      if (this.state.slider >= 0.99) {
        newState.slider = 0;
      }
      this.setState(newState);
      this.timer = setInterval(this.animatePlay, 32);
    } else {
      // Pause.
      this.setState({isPlaying: false});
      clearInterval(this.timer);
      this.timer = null;
    }
  };

  animatePlay = () => {
    if (this.state.slider >= 0.99) {
      // Stop playing.
      this.setState({slider: 1, isPlaying: false});
      clearInterval(this.timer);
      this.timer = null;
    } else {
      this.setState({slider: this.state.slider + 0.01});
    }
  };

  handleDownload = () => {
    if (!this.state.srcs || this.state.srcs.length < 1) {
      this.props.getSnackBar().open('info', 'No interpolated frames to save.');
      return;
    }
    const numSrcs = this.state.srcs.length;
    let zip = new JSZip();
    for (let i = 0; i < numSrcs; ++i) {
      zip.file('img'+i+'.png', this.state.srcs[i].split(',')[1], {base64: true});
    }
    zip.generateAsync({type:'blob'})
      .then(function(content) {
        saveAs(content, 'images.zip');
      });
  };

  render() {
    const active = this.getActiveImg() + 1;
    const total = this.state.srcs.length;
    let loadingStyle = Object.assign({}, styles.loading);
    if (this.state.isLoading) {
      loadingStyle.opacity = 1;
      loadingStyle.pointerEvents = 'all';
    }
    return (
      <div id='hint-view' style={styles.layout}>
        <div style={styles.canvasContainer}>
          <canvas ref="icanvas"
            onTouchStart={(e) => {this.handleMouseDown(e.nativeEvent)}}
            onTouchEnd={(e) => {this.handleMouseUp(e.nativeEvent)}}
            onTouchMove={(e) => {this.handleMouseMove(e.nativeEvent)}}
            onMouseDown={this.handleMouseDown}
            onMouseUp={this.handleMouseUp}
            onMouseMove={this.handleMouseMove}
            onDoubleClick={this.handleDoubleClick}
            onWheel={this.handleScroll}
            style={styles.canvas}
            tabIndex={0}
          />
          <CanvasCover show={this.state.srcs.length === 0}
            message="Interpolator hasn't been run yet."/>
        </div>
        <Divider/>
        <div style={styles.controlsContainer} ref="controlsContainer" tabIndex={0}>
          <div style={{display: 'flex', padding:'1px'}}>
            <Tooltip title={this.state.isPlaying ? 'Pause' : 'Play'} placement="right">
              <IconButton style={{flex: '0'}} onClick={this.handlePlay}>
                {this.state.isPlaying ?
                <PauseIcon fontSize="default"/> : <PlayArrowIcon fontSize="default"/>}
              </IconButton>
            </Tooltip>
            <div style={styles.sliderContainer}>
              <Slider
                style={styles.slider}
                value={this.state.slider * 100}
                onChange={this.handleSlider}
              />
            </div>
            <div style={styles.textContainer}>
              <Typography noWrap style={{flex: '0 0 auto', textAlign: 'center'}}>
                {active} of {total}
              </Typography>
            </div>
            <Tooltip title='Download images' placement='right'>
              <IconButton style={{flex: '0'}} onClick={this.handleDownload}>
                <CloudDownload fontSize='default' />
              </IconButton>
            </Tooltip>
          </div>
        </div>
        <img ref='img' onLoad={this.redraw} style={{display: 'none'}} />
        <div style={loadingStyle} ref="loading">
          <CircularProgress style={styles.loadingSpinner}/>
        </div>
      </div>
    );
  }
};
