import React from 'react';
import Typography from '@material-ui/core/Typography';

import BasicDialog from './BasicDialog';

export default class AboutDialog extends React.Component {
  open = () => {
    this.refs.dialog.open();
  }

  render() {
    return (
      <BasicDialog ref='dialog' title="About" closeText="Close">
        <Typography gutterBottom>
          2D Animation Inbetweener
        </Typography>
        <Typography gutterBottom>
          MIT licensed open source project: <a target="_blank" href="https://gitlab.com/frame-interpolator">frame-interpolator</a>.
        </Typography>
      </BasicDialog>
    );
  }
}

