import React from 'react';
import Switch from '@material-ui/core/Switch';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';

import BasicDialog from './BasicDialog';
import { MenuItem, FormControl, Select } from '@material-ui/core';

const styles = {
    contentContainer: {
        width: '450px',
    },
};

export default class InterpSettingsDialog extends React.Component {
    state = {
        getFlowVisualizations: true,
        getMiddleburyWarps: false,
        getBidirWarps: false,
        resolutionFraction: 1.0,
        quadraticMotion: false,
    };

    open = () => {
        this.refs.dialog.open();
    };

    getState = () => {
        return this.state;
    };

    render() {
    return (
        <BasicDialog ref='dialog' title="Interpolator Settings" closeText="Save">
            <div style={styles.contentContainer}>
                <List subheader={<ListSubheader>Options</ListSubheader>}>
                    <ListItem>
                        <ListItemText primary="Flow resolution fraction"
                            secondary="Runs the optical flow network at a lower resolution. Interpolates at full resolution."/>
                        <ListItemSecondaryAction>
                            <FormControl>
                                <Select value={this.state.resolutionFraction}
                                    onChange={(e) => {this.setState({resolutionFraction: e.target.value})}}
                                    displayEmpty name="resolutionFraction">
                                    <MenuItem value={1.0}>1</MenuItem>
                                    <MenuItem value={0.5}>1/2</MenuItem>
                                    <MenuItem value={0.25}>1/4</MenuItem>
                                    <MenuItem value={0.125}>1/8</MenuItem>
                                </Select>
                            </FormControl>
                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Quadratic motion"
                            secondary="Optimizes for low deformity using quadratic motion."/>
                        <ListItemSecondaryAction>
                            <Switch
                                onChange={() => {this.setState({quadraticMotion: !this.state.quadraticMotion})}}
                                checked={this.state.quadraticMotion}/>
                        </ListItemSecondaryAction>
                    </ListItem>
                </List>
                <List subheader={<ListSubheader>Advanced view options</ListSubheader>}>
                    <ListItem>
                        <ListItemText primary="Flow visualizations"/>
                        <ListItemSecondaryAction>
                            <Switch
                                onChange={() => {this.setState({getFlowVisualizations: !this.state.getFlowVisualizations})}}
                                checked={this.state.getFlowVisualizations}/>
                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Middlebury warps"
                            secondary="Runs the middlebury warping algorithm at t=0.5."/>
                        <ListItemSecondaryAction>
                            <Switch
                                onChange={() => {this.setState({getMiddleburyWarps: !this.state.getMiddleburyWarps})}}
                                checked={this.state.getMiddleburyWarps}/>
                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Bidirectional warps"
                            secondary="Runs the bidirectional warping algorithm at t=0.5."/>
                        <ListItemSecondaryAction>
                            <Switch
                                onChange={() => {this.setState({getBidirWarps: !this.state.getBidirWarps})}}
                                checked={this.state.getBidirWarps}/>
                        </ListItemSecondaryAction>
                    </ListItem>
                </List>
            </div>
        </BasicDialog>
    );
    }
}

