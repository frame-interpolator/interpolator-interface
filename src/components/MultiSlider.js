import React from 'react';

import EasyGL from '../utils/EasyGL';

const styles = {
  canvas: {
    height: '60px',
    width: '100%',
    outline: 'none',
  },
  container: {
    width: '100%',
    padding: '10px',
  },
};

const valueRadius = 10;

export default class MultiSlider extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      values: []
    };

    this.padLeft = 10;
    this.padTop = 5;
  }
      
  /***************************************************************************
   * Public Accessor                                                         *
   ***************************************************************************/
  getValues = () => {
    // Return a sorted copy of the array.
    return this.state.values.slice().sort();
  };

  setValues = (values) => {
    this.setState({values: values}, this.redraw);
  };

  /***************************************************************************
   * Lifecycle functions                                                     *
   ***************************************************************************/
  componentDidMount() {
    this.easyGL = new EasyGL(this.refs.scanvas.getContext('2d'));
    window.addEventListener("resize", this.handleResize);
    document.addEventListener("keydown", this.handleKeyDown);
    this.handleResize();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
    document.removeEventListener("keydown", this.handleKeyDown);
  }

  componentDidUpdate() {
    this.redraw();
  }

  handleResize = () => {
    this.easyGL.resize_canvas();
    this.redraw();
  };

  handleKeyDown = (event) => {
    if (this.props.locked) {
      return;
    }
    const activeEl = document.activeElement;
    if (activeEl === this.refs.scanvas && !!event.key) {
      if (event.key === 'Delete') {
        this.deleteSelected();
      }
    }
  };

  redraw = () => {
    this.easyGL.clearscreen();
    const padTop = this.padTop;
    let xleft;
    let xright;
    let ymid;
    [xleft, ymid] = this.mapValueToSlider(0);
    [xright, ymid] = this.mapValueToSlider(1);
    const height = ymid * 2;
    // Draw base slider.
    this.easyGL.drawline(xleft, padTop, xleft, height-padTop);
    this.easyGL.drawline(xright, padTop, xright, height-padTop);
    this.easyGL.drawline(xleft, ymid, xright, ymid);

    // Draw all points.
    const valueLen = this.state.values.length;
    for (let i = 0; i < valueLen; ++i) {
      const [x, y] = this.mapValueToSlider(this.state.values[i]);
      const oldFillStyle = this.easyGL.setFillStyle('#3f51b5');
      this.easyGL.drawPolygon([
        {x: x, y: y},
        {x: x - valueRadius, y: y + valueRadius},
        {x: x + valueRadius, y: y + valueRadius},
      ]);
      this.easyGL.setFillStyle(oldFillStyle);
      if (i === this.state.selected) {
        this.easyGL.drawboxedtext(x, ymid/2, Math.round(this.state.values[i] * 100) / 100);
      }
    }
  };
    
  /***************************************************************************
   * Event Utilities                                                         *
   ***************************************************************************/
  pageToCanvasCoord = (x, y) => {
    const rect = this.refs.scanvas.getBoundingClientRect();
    return [(x - rect.left), (y - rect.top)];
  };

  mapValueToSlider = (value) => {
    const padLeft = this.padLeft;
    const [width, height] = this.easyGL.get_size();
    return [value * (width - padLeft * 2) + padLeft, height/2]
  };

  mapSliderToValue = (x, y) => {
    const padLeft = this.padLeft;
    const [width, height] = this.easyGL.get_size();
    const value = (x - padLeft) / (width - padLeft * 2);
    if (value < 0) return 0;
    if (value > 1) return 1;
    return value;
  };

  /***************************************************************************
   * Mouse Events                                                            *
   ***************************************************************************/
  eventToCoord = (event) => {
    let coords = null;
    if (!!event.touches) {
      if (event.touches.length === 0) {
        return null;
      }
      coords = this.pageToCanvasCoord(event.touches[0].clientX, event.touches[0].clientY);
    } else {
      coords = this.pageToCanvasCoord(event.clientX, event.clientY);
    }
    return coords;
  };
  
  handleMouseDown = (event) => {
    let coords = this.eventToCoord(event);
    if (!coords) {
      return;
    }
    const [x, y] = coords;

    // Check if it selected a value.
    const valueLen = this.state.values.length;
    for (let i = 0; i < valueLen; ++i) {
      const [valuex, valuey] = this.mapValueToSlider(this.state.values[i]);
      if (Math.abs(valuex - x) < valueRadius) {
        this.setState({
          dragging: true,
          selected: i
        });
        return;
      }
    }

    // Locked state allows selection to change, but not create new values.
    if (this.props.locked) {
      return;
    }
    
    // Didn't click on anything. Create a new slider point.
    this.setState({
      dragging: true,
      selected: this.state.values.length,
      values: this.state.values.concat([this.mapSliderToValue(x, y)])
    });
  };

  handleMouseUp = (event) => {
    this.setState({dragging: undefined}, () => {
      // Manual changes no allowed during lock.
      if (!this.props.locked) {
        this.onChanged();
      }
    });
  };

  handleMouseMove = (event) => {
    if (this.props.locked) {
      return;
    }

    if (this.state.dragging) {
      let coords = this.eventToCoord(event);
      if (!coords) {
        return;
      }
      const [x, y] = coords;
      // Somehow I like this more than calling setState.
      this.state.values[this.state.selected] = this.mapSliderToValue(x, y);
      this.redraw();
      this.onChange();
    }
  };

  deleteSelected = () => {
    if (this.props.locked) {
      return;
    }

    if (typeof this.state.selected !== 'undefined') {
      this.state.values.splice(this.state.selected, 1);
      this.setState({
        selected: undefined
      }, this.onChanged);
    }
  };

  handleDoubleClick = (event) => {
    if (this.props.locked) {
      return;
    }

    this.deleteSelected();
  };

  /**
   * Called while manually changing.
   */
  onChange = () => {
    if (this.props.onChange) {
      this.props.onChange(this.state.values);
    }
  };

  /**
   * Called once the manual change has completed.
   */
  onChanged = () => {
    this.onChange();
    if (this.props.onChanged) {
      this.props.onChanged(this.state.values);
    }
  };

  render() {
    return (
      <div id='multi-slider' style={styles.container}>
        <canvas ref="scanvas"
          onTouchStart={(e) => {this.handleMouseDown(e.nativeEvent)}}
          onTouchEnd={(e) => {this.handleMouseUp(e.nativeEvent)}}
          onTouchMove={(e) => {this.handleMouseMove(e.nativeEvent)}}
          onMouseDown={this.handleMouseDown}
          onMouseUp={this.handleMouseUp}
          onMouseMove={this.handleMouseMove}
          onDoubleClick={this.handleDoubleClick}
          style={styles.canvas}
          tabIndex={0}
        />
      </div>
    );
  }
};
