import React from 'react';

import EasyGL from '../utils/EasyGL';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import SettingsIcon from '@material-ui/icons/Settings';

import CanvasCover from './CanvasCover';
import InterpSettingsDialog from './InterpSettingsDialog';
import Toolbar from './Toolbar';

const styles = {
  layout: {
    height: '100%',
    width: '100%',
    display: 'inline-flex',
    flexDirection: 'column',
    position: 'relative',
  },
  canvasWrapper: {
    // flex-basis: 0% and minHeight: 0 allows canvas to shrink.
    flex: '1 1 0%',
    minHeight: '0',
    minWidth: '0',
    display: 'flex',
    position: 'relative',
  },
  canvas: {
    // flex-basis: 0% and minHeight: 0 allows canvas to shrink.
    flex: '1 1 0%',
    height: '100%',
    width: '100%',
    outline: 'none',
  },
  canvasContainer: {
    height: '100%',
    display: 'flex',
    flex: '1',
    minHeight: 0,
  },
  toolbarStyle: {
    position: 'absolute',
    zIndex: 10,
    top: '30px',
    left: '20px',
  }
};

const hintRadius = 5;
const actionCtxName = 'HintView';

class CloudUploadButton extends React.Component {
  render() {
    return (
      <Tooltip title="Load image" placement="right">
        <IconButton onClick={this.props.onUploadClick}>
          <CloudUploadIcon fontSize="large"/>
        </IconButton>
      </Tooltip>
    );
  }
}

export default class HintView extends React.Component {
  constructor(props) {
    super(props);

    // Set initial state.
    this.state = {
      orientation: 'column',
      hints: [],
      loaded: [false, false],
      loading: false,
      canvasCursor: 'unset',
    };
  }

  /***************************************************************************
   * Lifecycle functions                                                     *
   ***************************************************************************/
  componentDidMount() {
    this.easyGL = [
      new EasyGL(this.refs.canvas0.getContext('2d')),
      new EasyGL(this.refs.canvas1.getContext('2d'))
    ];
    window.addEventListener("resize", this.handleResize);
    document.addEventListener("keydown", this.handleKeyDown);
    this.handleResize();
    if (!this.props.getActionStack().registerUndoRedoCallback(actionCtxName,
        this.handleUndoRedoCallback, this.state.hints)) {
      console.error('HintView could not register callback.');
    }
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
    document.removeEventListener("keydown", this.handleKeyDown);
    if (!this.props.getActionStack().unregisterUndoRedoCallback(actionCtxName)) {
      console.error('HintView could not unregister callback.');
    }
  }

  componentDidUpdate() {
    this.redraw();
  }

  /**
   * @returns {boolean} True if the hint view is the active element.
   */
  isActive = () => {
    return !!document.activeElement && this.refs.hintView.contains(document.activeElement);
  };

  /**
   * Called when an undo/redo is triggered.
   * @param hints
   * @param context
   */
  handleUndoRedoCallback = (hints, context) => {
    if (context === actionCtxName && hints !== null) {
      this.setState({
        hints: hints,
      }, this.redraw());
    }
  };

  /**
   * Call this after modifying hints, with the exception of undoing and redoing.
   */
  addAction = () => {
    this.props.getActionStack().addState(this.state.hints, actionCtxName);
  };

  handleResize = () => {
    this.easyGL[0].resize_canvas();
    this.easyGL[1].resize_canvas();
    this.forceUpdate();
  };

  onLoadImg0 = () => {
    if (!this.img) {
      this.img = [];
    }
    let newState = Object.assign({}, this.state);
    newState.loaded[0] = true;
    this.setState(newState);
    this.img[0] = this.refs.img0;
    this.redraw();
  };

  onLoadImg1 = () => {
    if (!this.img) {
      this.img = [];
    }
    let newState = Object.assign({}, this.state);
    newState.loaded[1] = true;
    this.setState(newState);
    this.img[1] = this.refs.img1;
    this.redraw();
  };

  redraw = () => {
    if (!this.easyGL) {
      return;
    }

    const numCanvases = this.easyGL.length;
    for (var cid = 0; cid < numCanvases; ++cid) {
      if (!this.easyGL[cid]) {
        return;
      }
      this.easyGL[cid].clearscreen();
      if (!this.img || !this.img[cid]) {
        if (this.state.loaded[cid]) {
          let newState = Object.assign({}, this.state);
          newState.loaded[cid] = false;
          this.setState(newState);
        }
        continue;
      }

      // Draw image overlay.
      try {
        this.easyGL[cid].drawimage(this.img[cid], 0, 0);
      } catch (error) {
        if (this.state.loaded[cid]) {
          let newState = Object.assign({}, this.state);
          newState.loaded[cid] = false;
          this.setState(newState);
        }
      }
    }

    const drawUnselectedHint = (cid, x, y, lightRingColour, darkRingColor) => {
      const oldColour = this.easyGL[cid].setStrokeStyle(lightRingColour);
      this.easyGL[cid].drawscreencircle(x, y, hintRadius-1);
      this.easyGL[cid].setStrokeStyle(darkRingColor);
      this.easyGL[cid].drawscreencircle(x, y, hintRadius);
      this.easyGL[cid].setStrokeStyle(oldColour);
    };

    const drawSelectedHint = (cid, x, y, lightRingColour, darkRingColor) => {
      const oldFillColour = this.easyGL[cid].setFillStyle(darkRingColor);
      this.easyGL[cid].fillscreencircle(x, y, hintRadius);
      this.easyGL[cid].setFillStyle(oldFillColour);
      const oldStrokeColour = this.easyGL[cid].setStrokeStyle(lightRingColour);
      this.easyGL[cid].drawscreencircle(x, y, hintRadius+1);
      this.easyGL[cid].setStrokeStyle(oldStrokeColour);
    };

    // Draw all hints.
    const hintLen = this.state.hints.length;
    for (let i = 0; i < hintLen; ++i) {
      const hint = this.state.hints[i];
      const drawFunc = (i === this.state.selected) ? drawSelectedHint : drawUnselectedHint;
      if (hint.imageId !== undefined) {
        drawFunc(hint.imageId, hint.x, hint.y, '#FFFFFF', '#550000');
      } else {
        drawFunc(0, hint.x, hint.y, '#FFFFFF', '#000000');
        drawFunc(1, hint.x + hint.dx, hint.y + hint.dy, '#FFFFFF', '#000000');
      }
    }
  };

  /***************************************************************************
   * Event Utilities                                                         *
   ***************************************************************************/
  saveHint = () => {
    if (!this.bothImagesLoaded()) {
      this.props.getSnackBar().open('warning', 'Cannot save hints. Images are not loaded yet.');
      return;
    }
    if (this.state.hints.length === 0) {
      this.props.getSnackBar().open('info', 'No hints to be saved.');
      return;
    }
    const text = JSON.stringify(this.state.hints);
    const filename = 'hints.txt';
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  };

  loadHint = () => {
    if (!this.bothImagesLoaded()) {
      this.props.getSnackBar().open('warning', 'Cannot load hints. Images are not loaded yet.');
      return;
    }
    this.refs.hint_reader.click();
  };

  pageToCanvasCoord = (cid, x, y) => {
    const canvas = (cid === 0 ? this.refs.canvas0 : this.refs.canvas1);
    const rect = canvas.getBoundingClientRect();
    return [(x - rect.left), (y - rect.top)]
  };

  replicateTransform = (cid) => {
    this.easyGL[1-cid].copy_transform(this.easyGL[cid]);
  };

  bothImagesLoaded = () => {
    return !!this.state.loaded && this.state.loaded[0] && this.state.loaded[1];
  };

  /***************************************************************************
   * Events                                                                  *
   ***************************************************************************/

  eventToCoord = (cid, event) => {
    let coords = null;
    if (!!event.touches) {
      if (event.touches.length === 0) {
        return null;
      }
      coords = this.pageToCanvasCoord(cid, event.touches[0].clientX, event.touches[0].clientY);
    } else {
      coords = this.pageToCanvasCoord(cid, event.clientX, event.clientY);
    }
    return coords;
  };

  handleMouseDown = (cid, event) => {
    const mode = this.refs.toolbar.getMode();
    let coords = this.eventToCoord(cid, event);
    if (!coords) {
      return;
    }
    const [x, y] = coords;

    // Check if it selected a hint.
    if (mode === 'default' || mode === 'move' || mode === 'remove') {
      const hintLen = this.state.hints.length;
      for (var i = 0; i < hintLen; ++i) {
        const hint = this.state.hints[i];
        if (hint.imageId !== undefined && hint.imageId !== cid) {
          continue;
        }
        const [hintx, hinty] = hint.imageId === undefined ?
            this.easyGL[cid].world_to_screen(
              hint.x + hint.dx * cid, hint.y + hint.dy * cid) :
            this.easyGL[hint.imageId].world_to_screen(
              hint.x, hint.y);
        if (Math.abs(hintx - x) + Math.abs(hinty - y) < hintRadius * 3 &&
          this.bothImagesLoaded()) {
          this.dragging = {
            cid: cid,
            totaldx: 0,
            totaldy: 0,
            prevx: x,
            prevy: y,
            mode: mode,
          };
          this.setState({selected: i});
          return;
        }
      }
    }
    this.panning = {
      cid: cid,
      prevx: x,
      prevy: y,
      total: 0,
      mode: mode,
    };
  };

  handleMouseUp = (cid, event) => {
    if (this.panning) {
      if (!this.panning.panned && this.bothImagesLoaded() &&
          (this.panning.mode === 'default' || this.panning.mode === 'add')) {
        // Create a new hint.
        let coords = this.eventToCoord(this.panning.cid, event);
        if (!coords) {
          return;
        }
        let [x, y] = coords;
        [x, y] = this.easyGL[this.panning.cid].screen_to_world(x, y);
        let newHint = {
          x: x,
          y: y,
        };
        if (event.ctrlKey) {
          // This hint is only available for the current canvas.
          newHint.imageId = this.panning.cid;
        } else {
          newHint.dx = 0;
          newHint.dy = 0;
        }
        this.setState({
          selected: this.state.hints.length,
          hints: this.state.hints.concat([newHint])
        }, this.addAction);
      }
      delete this.panning;
    }
    if (this.dragging) {
      if (this.bothImagesLoaded() && this.dragging.mode === 'remove') {
        this.deleteSelectedHint();
      } else if (Math.abs(this.dragging.totaldx) > 1e-3 || Math.abs(this.dragging.totaldy) > 1e-3) {
        // If the hint moved, then it was modified.
        this.addAction();
      }
      delete this.dragging;
    }
  };

  handleMouseMove = (cid, event) => {
    if (this.panning && (this.panning.mode === 'default' || this.panning.mode === 'pan')) {
      let coords = this.eventToCoord(this.panning.cid, event);
      if (!coords) {
        return;
      }
      const [x, y] = coords;
      // Do everything relative to original canvas.
      const dx = x - this.panning.prevx;
      const dy = y - this.panning.prevy;
      this.panning.prevx = x;
      this.panning.prevy = y;
      if (!this.panning.panned) {
        this.panning.total += Math.abs(dx) + Math.abs(dy);
        if (this.panning.total > 5) {
          this.panning.panned = new Boolean('true');
        }
      }
      this.easyGL[this.panning.cid].translate_screen(dx, dy);
      this.replicateTransform(this.panning.cid);
      this.redraw();
    }
    if (this.dragging && (this.dragging.mode === 'default' || this.dragging.mode === 'move')) {
      let coords = this.eventToCoord(this.dragging.cid, event);
      if (!coords) {
        return;
      }
      const [x, y] = coords;
      // Do everything relative to the original canvas.
      const dx = (x - this.dragging.prevx) / this.easyGL[this.dragging.cid].get_scale();
      const dy = (y - this.dragging.prevy) / this.easyGL[this.dragging.cid].get_scale();
      this.dragging.prevx = x;
      this.dragging.prevy = y;
      
      let hint = this.state.hints[this.state.selected];
      if (hint.imageId !== undefined && hint.imageId === this.dragging.cid) {
        hint.x += dx;
        hint.y += dy;
      } else {
        if (this.dragging.cid === 0) {
          // Move hint start but not end.
          hint.x += dx;
          hint.y += dy;
          hint.dx -= dx;
          hint.dy -= dy;
        } else {
          // Move hint end but not start.
          hint.dx += dx;
          hint.dy += dy;
        }
      }
      this.dragging.totaldx += dx;
      this.dragging.totaldy += dy;
      this.redraw();
    }
  };

  handleScroll = (cid, event) => {
    const [x, y] = this.pageToCanvasCoord(cid, event.clientX, event.clientY);
    if (event.deltaY < 0) {
      // Scrolling up.
      this.easyGL[cid].zoom_in_to_screen(x, y);
    } else if (event.deltaY > 0) {
      // Scrolling down.
      this.easyGL[cid].zoom_out_at_screen(x, y);
    } else {
      return;
    }
    this.replicateTransform(cid);
    this.redraw();
  };

  deleteSelectedHint = () => {
    if (this.state.selected !== undefined &&
        this.state.selected < this.state.hints.length && this.state.selected >= 0) {
      // Must remove from out here, since splice returns the removed items.
      this.state.hints.splice(this.state.selected, 1);
      this.setState({
        selected: undefined,
        hints: this.state.hints,
      }, this.addAction);
    }
  };

  handleDoubleClick = (event) => {
    if (this.refs.toolbar.getMode() === 'default') {
      this.deleteSelectedHint();
    }
  };

  handleModeChange = (mode) => {
    let newState = 'unset';
    if (mode === 'pan') {
      newState = 'grab';
    } else if (mode === 'move') {
      newState = 'move';
    }
    if (this.state.canvasCursor !== newState) {
      this.setState({canvasCursor: newState});
    }
  };

  handlePaste = (event) => {
    const parseImage = (event, cid) => {
      const items = (event.clipboardData  || event.originalEvent.clipboardData).items;
      let file = null;
      for (let i = 0; i < items.length; ++i) {
        const item = items[i];
        if (item.type.indexOf('image') === 0) {
          file = item.getAsFile();
          break;
        }
      }
      if (file !== null) {
        this.loadImageFromFile(cid, file);
      }
    };

    // Use active element.
    const activeEl = document.activeElement;
    if (activeEl === this.refs.canvas0) {
      parseImage(event, 0);
    } else if (activeEl === this.refs.canvas1) {
      parseImage(event, 1);
    } else if (!this.bothImagesLoaded()) {
      // If no active element, try pasting into one of the unloaded images (if any).
      if (!this.state.loaded[0]) {
        parseImage(event, 0);
      } else {
        parseImage(event, 1);
      }
    }
  };

  /***************************************************************************
   * Button Events                                                           *
   ***************************************************************************/
  handleKeyDown = (event) => {
    const activeEl = document.activeElement;
    if (this.isActive() && !!event.key) {
      if (event.key === 'Delete') {
        this.deleteSelectedHint();
      }
    }
  };

  handleClearHints = (event) => {
    this.setState({hints: []}, this.addAction);
  };

  handleRunInterpolator = (event) => {
    if (!this.img || !this.img[0] || !this.img[1] || !this.img[0].src || !this.img[1].src) {
      this.props.getSnackBar().open('warning', 'Cannot run interpolator. Images not loaded.');
      return;
    }
    const sliderValues = this.props.getTimeSlider().getValues();
    const body = {
      img0: this.img[0].src,
      img1: this.img[1].src,
      times: sliderValues,
      resolution_fraction: this.refs.interpSettingsDialog.getState().resolutionFraction,
      get_flow_visualizations: this.refs.interpSettingsDialog.getState().getFlowVisualizations,
      get_middlebury_interp: this.refs.interpSettingsDialog.getState().getMiddleburyWarps,
      get_bidir_interp: this.refs.interpSettingsDialog.getState().getBidirWarps,
      quadratic_motion: this.refs.interpSettingsDialog.getState().quadraticMotion,
    };
    if (this.state.hints.length > 0) {
      body['hints'] = this.state.hints;
    }
    this.props.getViewer().startLoading();
    this.setState({loading: true});
    fetch('echo', {
        method: 'post',
        headers: {
            'Content-type': 'application/x-www-form-urlencoded'
        }, 
        body: JSON.stringify(body)
      })
      .then(status)
      .then((response) => {return response.text()})
      .then(this.receiveServerResponse)
      .catch((error) => {
          this.props.getSnackBar().open('error',
          'Request failed: ' + error);
      })
      .finally(() => {
        this.props.getViewer().stopLoading();
        this.setState({loading: false});
      });
  };

  receiveServerResponse = (data) => {
    var parsed = JSON.parse(data);
    parsed.images.unshift(this.img[0].src);
    parsed.images.push(this.img[1].src);
    this.props.getViewer().setSrcs(parsed.images);
    this.updateAdvancedView(parsed);
    this.props.getSnackBar().open('success', 'Frames interpolated.');
  };

  updateAdvancedView = (parsed) => {
    let advancedViewState = {images: []};
    if (!!parsed.flow_visualizations && parsed.flow_visualizations.length === 2) {
      advancedViewState.images.push({
        name: 'Forward flow',
        src: parsed.flow_visualizations[0],
      });
      advancedViewState.images.push({
        name: 'Backward flow',
        src: parsed.flow_visualizations[1],
      });
    }
    if (!!parsed.forward_flow_midpoints && parsed.forward_flow_midpoints.length === 2) {
      advancedViewState.images.push({
        name: 'Forward flow midpoints',
        src: parsed.forward_flow_midpoints[0],
      });
      advancedViewState.images.push({
        name: 'Backward flow midpoints',
        src: parsed.forward_flow_midpoints[1],
      });
    }
    if (!!parsed.foward_interped_middlebury) {
      advancedViewState.images.push({
        name: 'Forward middlebury',
        src: parsed.foward_interped_middlebury,
      });
    }
    if (!!parsed.backward_interped_middlebury) {
      advancedViewState.images.push({
        name: 'Backward middlebury',
        src: parsed.backward_interped_middlebury,
      });
    }
    if (!!parsed.forward_interped_bidir) {
      advancedViewState.images.push({
        name: 'Forward bidir interp warp',
        src: parsed.forward_interped_bidir,
      });
    }
    if (!!parsed.backward_interped_bidir) {
      advancedViewState.images.push({
        name: 'Backward bidir interp warp',
        src: parsed.backward_interped_bidir,
      });
    }
    this.props.setAdvancedViewState(advancedViewState);
  };

  readHints = (event) => {
    const file = event.target.files[0];
    const fReader = new FileReader();
    const capture = this;
    fReader.onload = function(evt) {
      capture.setState({
        hints: JSON.parse(evt.target.result)
      }, capture.addAction);
    };
    fReader.readAsText(file);
  };

  readImage = (cid, event) => {
    const file = event.target.files[0];
    const fileType = file['type'];
    const validImageTypes = ['image/jpeg', 'image/png'];
    if (!validImageTypes.includes(fileType)) {
      this.props.getSnackBar().open('error', 'File was not an image.');
      return;
    }
    this.loadImageFromFile(cid, file);
  };

  loadImageFromFile = (cid, file) => {
    const fReader = new FileReader();
    if (!this.img) {
      // Hacky way to handle cases where images weren't loaded.
      this.img = [];
      this.img[0] = this.refs.img0;
      this.img[1] = this.refs.img1;
    }
    fReader.onload = (evt) => {
      this.state.loaded[cid] = true;
      this.img[cid].src = (evt.target.result);
    };
    fReader.readAsDataURL(file);
  };

  openInterpolatorSettings = () => {
    this.refs.interpSettingsDialog.open();
  };

  loadImage1 = () => {
    this.refs.img0_reader.click();
  };

  loadImage2 = () => {
    this.refs.img1_reader.click();
  };

  /***************************************************************************
   * HTTPS Request Helpers                                                   *
   ***************************************************************************/
  status = (response) => {
    if (response.status >= 200 && response.status < 300) {
      return Promise.resolve(response)
    } else {
      return Promise.reject(new Error(response.statusText))
    }
  };

  render() {
    // Variable flexDirection property of const style.
    var canvasContainerStyle = Object.assign(
        {flexDirection: this.state.orientation}, styles.canvasContainer);
    const gridSpacing = 8;
    let canvasStyle = Object.assign({}, styles.canvas, {cursor: this.state.canvasCursor});
    return (
      <div ref='hintView' style={styles.layout}>
        <div style={canvasContainerStyle}>
          <div style={styles.canvasWrapper}>
            <canvas ref="canvas0"
              onTouchStart={(e) => {this.handleMouseDown(0, e.nativeEvent)}}
              onTouchEnd={(e) => {this.handleMouseUp(0, e.nativeEvent)}}
              onTouchMove={(e) => {this.handleMouseMove(0, e.nativeEvent)}}
              onMouseDown={(e) => {this.handleMouseDown(0, e)}}
              onMouseUp={(e) => {this.handleMouseUp(0, e)}}
              onMouseMove={(e) => {this.handleMouseMove(0, e)}}
              onWheel={(e) => {this.handleScroll(0, e)}}
              onDoubleClick={(e) => {this.handleDoubleClick(e)}}
              style={canvasStyle}
              tabIndex={0}
            />
            <CanvasCover show={!this.state.loaded[0]} message={`Image 1 not loaded.`}>
              <CloudUploadButton onUploadClick={() => this.refs.img0_reader.click()}/>
            </CanvasCover>
          </div>
          <Divider/>
          <div style={styles.canvasWrapper}>
            <canvas ref="canvas1"
              onTouchStart={(e) => {this.handleMouseDown(1, e.nativeEvent)}}
              onTouchEnd={(e) => {this.handleMouseUp(1, e.nativeEvent)}}
              onTouchMove={(e) => {this.handleMouseMove(1, e.nativeEvent)}}
              onMouseDown={(e) => {this.handleMouseDown(1, e)}}
              onMouseUp={(e) => {this.handleMouseUp(1, e)}}
              onMouseMove={(e) => {this.handleMouseMove(1, e)}}
              onWheel={(e) => {this.handleScroll(1, e)}}
              onDoubleClick={(e) => {this.handleDoubleClick(e)}}
              style={canvasStyle}
              tabIndex={0}
            />
            <CanvasCover show={!this.state.loaded[1]} message={`Image 2 not loaded.`}>
              <CloudUploadButton onUploadClick={() => this.refs.img1_reader.click()}/>
            </CanvasCover>
          </div>
        </div>
        <img ref='img0' onLoad={this.onLoadImg0} style={{display: 'none'}}/>
        <img ref='img1' onLoad={this.onLoadImg1} style={{display: 'none'}}/>
        <input ref='img0_reader'
          type='file'
          onChange={this.readImage.bind(this, 0)}
          style={{display: 'none'}} />
        <input ref='img1_reader'
          type='file'
          onChange={this.readImage.bind(this, 1)}
          style={{display: 'none'}} />
        <input ref='hint_reader'
          type='file'
          onChange={this.readHints}
          style={{display: 'none'}} />
        <a href='' download='hint.json' id='hint_saver'/>
        <Divider/>
        <div style={{flex: '0 1 auto', padding: gridSpacing + 'px', backgroundColor: 'white'}}>
          <Grid container direction='column' spacing={gridSpacing}>
            <Grid item>
              <Grid container direction='row' spacing={gridSpacing}>
                <Grid item>
                  <Button variant="outlined" onClick={this.loadImage1}>
                    Load Image 1
                  </Button>
                </Grid>
                <Grid item>
                  <Button variant="outlined"  onClick={this.loadImage2}>
                    Load Image 2
                  </Button>
                </Grid>
                <Grid item>
                  <Button variant="outlined" onClick={this.handleClearHints}>
                    Clear Hints
                  </Button>
                </Grid>
                <Grid item>
                  <Button variant="outlined" id='runbtn' onClick={this.handleRunInterpolator}
                    disabled={this.state.loading}>
                    Run Interpolator
                  </Button>
                </Grid>
                <Grid item>
                  <div style={{margin: `${-gridSpacing / 2}px`}}>
                    <Tooltip title="Interpolator settings" placement="right">
                      <IconButton onClick={this.openInterpolatorSettings}>
                        <SettingsIcon fontSize="small"/>
                      </IconButton>
                    </Tooltip>
                  </div>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>
        <InterpSettingsDialog ref='interpSettingsDialog'/>
        <Toolbar ref="toolbar" onModeChange={this.handleModeChange}
          style={styles.toolbarStyle} disabled={!this.bothImagesLoaded()}/>
      </div>
    );
  }
}
