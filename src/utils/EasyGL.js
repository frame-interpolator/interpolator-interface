const ZOOM_FACTOR = 1.1;

export default class EasyGL {
  constructor(ctx) {
    this.ctx = ctx;
    this.text_height = 12;
    this.ctx.font = this.text_height + 'pt Arial';
  }

  reset(ctx) {
    this.ctx = ctx;
  }

  clearscreen() {
    this.ctx.save();
    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this.ctx.restore();
  }

  get_size() {
    return [this.ctx.canvas.width, this.ctx.canvas.height];
  }

  get_transform() {
    if (this.ctx.getTransform) {
      // Chrome's canvas.
      return this.ctx.getTransform();
    } else if (this.ctx.mozCurrentTransform) {
      // Firefox's canvas.
      return new DOMMatrix(this.ctx.mozCurrentTransform);
    } else {
      // Who knows, but the specs say this should exist.
      return this.ctx.currentTransform;
    }
  }

  set_transform(mat) {
    if (this.ctx.getTransform) {
      // Chrome's canvas.
      this.ctx.setTransform(mat);
    } else if (this.ctx.mozCurrentTransform) {
      // Firefox's canvas.
      this.ctx.setTransform(mat.a, mat.b, mat.c, mat.d, mat.e, mat.f);
    } else {
      // Who knows, but the specs say this should exist.
      this.ctx.currentTransform = mat;
    }
  }

  copy_transform(easyGL) {
    this.set_transform(easyGL.get_transform());
  }

  resize_canvas() {
    var canvas = this.ctx.canvas;
    const rect = canvas.getBoundingClientRect();
    const mat = this.get_transform();
    canvas.width = rect.width;
    canvas.height = rect.height;
    this.set_transform(mat);
  }

  screen_to_world(screenx, screeny) {
    var mat = this.get_transform();
    mat.invertSelf();
    const x = screenx * mat.a + screeny * mat.c + mat.e;
    const y = screenx * mat.b + screeny * mat.d + mat.f;
    return [x, y];
  }

  world_to_screen(worldx, worldy) {
    const mat = this.get_transform();
    const x = worldx * mat.a + worldy * mat.c + mat.e;
    const y = worldx * mat.b + worldy * mat.d + mat.f;
    return [x, y];
  }

  translate_world(dx, dy) {
    this.ctx.translate(dx, dy);
  }

  get_scale() {
    return this.get_transform().a;
  }

  translate_screen(dx, dy) {
    // I couldn't figure out an easy way to find the current scale.
    const [x, y] = this.screen_to_world(dx, dy);
    const [x0, y0] = this.screen_to_world(0, 0);
    this.ctx.translate(x - x0, y - y0);
  }

  zoom_in_to_screen(x, y) {
    [x, y] = this.screen_to_world(x, y);
    var mat = this.get_transform();
    if (this.ctx.getTransform) {
      this.set_transform(mat.scale(ZOOM_FACTOR, ZOOM_FACTOR, 1, x, y));
    } else {
      this.set_transform(mat.scale(ZOOM_FACTOR, x, y));
    }
  }

  zoom_out_at_screen(x, y) {
    [x, y] = this.screen_to_world(x, y);
    var mat = this.get_transform();
    if (this.ctx.mozCurrentTransform) {
      // Firefox's DOMMatrix.
      this.set_transform(mat.scale(1/ZOOM_FACTOR, x, y));
    } else {
      // Hopefully everyone else's DOMMatrix.
      this.set_transform(mat.scale(1/ZOOM_FACTOR, 1/ZOOM_FACTOR, 1, x, y));
    }
  }

  setStrokeStyle(str) {
    const temp = this.ctx.strokeStyle;
    this.ctx.strokeStyle = str;
    return temp;
  }

  setFillStyle(str) {
    const temp = this.ctx.fillStyle;
    this.ctx.fillStyle = str;
    return temp;
  }

  drawPolygon(points) {
    if (points.length < 3) {
      return;
    }
    this.ctx.beginPath();
    this.ctx.moveTo(points[0].x, points[0].y);
    for (let i = 1; i < points.length; ++i) {
      this.ctx.lineTo(points[i].x, points[i].y);
    }
    this.ctx.closePath();
    this.ctx.fill();
  }

  drawline(startx, starty, endx, endy) {
    this.ctx.beginPath();
    this.ctx.moveTo(startx, starty);
    this.ctx.lineTo(endx, endy);
    this.ctx.stroke();
    this.ctx.closePath();
  }

  fillcircle(centerx, centery, radius) {
    this.ctx.beginPath();
    this.ctx.arc(centerx, centery, radius, 0, 2 * Math.PI);
    this.ctx.fill();
    this.ctx.closePath();
  }

  fillscreencircle(centerx, centery, radius) {
    this.ctx.beginPath();
    const scale = this.get_scale();
    this.ctx.arc(centerx, centery, radius / scale, 0, 2 * Math.PI);
    this.ctx.fill();
    this.ctx.closePath();
  }

  drawcircle(centerx, centery, radius) {
    this.ctx.beginPath();
    this.ctx.arc(centerx, centery, radius, 0, 2 * Math.PI);
    this.ctx.stroke();
    this.ctx.closePath();
  }

  drawscreencircle(centerx, centery, radius) {
    this.ctx.beginPath();
    const scale = this.get_scale();
    this.ctx.arc(centerx, centery, radius / scale, 0, 2 * Math.PI);
    const lineWidth = this.ctx.lineWidth;
    this.ctx.lineWidth = 1 / scale;
    this.ctx.stroke();
    this.ctx.lineWidth = lineWidth;
    this.ctx.closePath();
  }

  drawbezier(xstart, ystart, x1, y1, x2, y2, xend, yend) {
    this.ctx.beginPath();
    this.ctx.moveTo(xstart, ystart);
    this.ctx.bezierCurveTo(x1, y1, x2, y2, xend, yend);
    this.ctx.stroke();
    this.ctx.closePath();
  }

  drawimage(image, x, y, alpha = 1) {
    const saveAlpha = this.ctx.globalAlpha;
    this.ctx.globalAlpha = alpha;
    this.ctx.drawImage(image, x, y);
    this.ctx.globalAlpha = saveAlpha;
  }

  drawhint(hint) {
    this.drawline(hint.x, hint.y, hint.x + hint.dx, hint.y + hint.dy);
  }

  drawboxedtext(x, y, text) {
    const width = this.ctx.measureText(text).width;
    const height = this.text_height;
    const pad = 2;
    this.drawline(x - width/2 - pad, y - height/2 - pad,
                  x + width/2 + pad, y - height/2 - pad);
    this.drawline(x + width/2 + pad, y - height/2 - pad,
                  x + width/2 + pad, y + height/2 + pad);
    this.drawline(x + width/2 + pad, y + height/2 + pad,
                  x - width/2 - pad, y + height/2 + pad);
    this.drawline(x - width/2 - pad, y + height/2 + pad,
                  x - width/2 - pad, y - height/2 - pad);
    this.ctx.fillText(text, x - width/2, y + height/2 - pad);
  }

  bigtext(x, y, text) {
    const font = this.ctx.font;
    const text_height = 40;
    this.ctx.font = text_height + 'px Arial';
    this.ctx.fillText(text, x, y + text_height);
    this.ctx.font = font;
  }
}
