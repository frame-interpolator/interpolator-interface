const MAX_STACK_SIZE = 200;

export default class ActionStack {
    constructor() {
        /**
         * Stores undo redo stacks for each context.
         * @type {{context: string, undoStack: Array, redoStack: Array, callback: function, initialState: *}}
         * @private
         */
        this._contexts = {};
        /**
         * @type {Array}
         * @private
         */
        this._undoContextStack = [];
        /**
         * @type {Array}
         * @private
         */
        this._redoContextStack = [];
        /**
         * When the undo/redo stack sizes change.
         * @type {Set<function(int, int)>}
         * @private
         */
        this._countCallbacks = new Set();
    }

    /**
     * Reverts to the previous state and makes callbacks.
     */
    undo = () => {
        // The last element of the undo stack is always the current state. Therefore, prevent undoing if there is
        // only one element left.
        if (this._undoContextStack.length === 0) {
            return;
        }
        const ctx = this._undoContextStack[this._undoContextStack.length - 1];
        let undoStack = this._contexts[ctx].undoStack;
        let redoStack = this._contexts[ctx].redoStack;
        if (undoStack.length === 0) {
            return;
        }
        this._redoContextStack.push(this._undoContextStack.pop());
        redoStack.push(undoStack.pop());
        this._enforceMaxStackSize();
        this._doCountCallbacks();
        this._doUndoRedoCallbacks(ctx);
    };

    /**
     * Returns to the undone state and makes callbacks.
     */
    redo = () => {
        if (this._redoContextStack.length === 0) {
            return;
        }
        const ctx = this._redoContextStack[this._redoContextStack.length - 1];
        let undoStack = this._contexts[ctx].undoStack;
        let redoStack = this._contexts[ctx].redoStack;
        if (redoStack.length === 0) {
            return;
        }
        this._undoContextStack.push(this._redoContextStack.pop());
        undoStack.push(redoStack.pop());
        this._enforceMaxStackSize();
        this._doCountCallbacks();
        this._doUndoRedoCallbacks(ctx);
    };

    /**
     * Adds to the undo stack and clears the redo stack.
     * This should be called once when the ctx initializes so that there is an initial state.
     * @param state State to add to the undo stack. A deep copy of this state will be made.
     * @param ctx that the state came from. Should be a unique string identifier.
     */
    addState = (state, ctx) => {
        if (!(ctx in this._contexts)) {
            return;
        }
        // Push onto the corresponding undo stack and clear all redo stacks.
        this._contexts[ctx].undoStack.push(JSON.stringify(state));
        for (let key in this._contexts) {
            this._contexts[key].redoStack = [];
        }
        this._undoContextStack.push(ctx);
        this._redoContextStack = [];
        this._enforceMaxStackSize();
        this._doCountCallbacks();
    };

    /**
     * Register callbacks to receive updates. The callback should be idempotent.
     * @param callback Function with parameters (int: numUndo, int: numRedo).
     * @returns {boolean} Whether the callback was successfully registered.
     */
    registerCountCallback = (callback) => {
        if (this._countCallbacks.has(callback)) {
            return false;
        }
        this._countCallbacks.add(callback);
        return true;
    };

    /**
     * @param callback Function with parameters (int: numUndo, int: numRedo).
     * @returns {boolean} Whether the callback was registered before deleting.
     */
    unregisterCountCallback = (callback) => {
        if (!this._countCallbacks.has(callback)) {
            return false;
        }
        this._countCallbacks.delete(callback);
        return true;
    };

    /**
     * Register callbacks to receive updates. The callback should be idempotent.
     * @param ctx String describing the context.
     * @param callback Function with parameters (null|*: state, string: context).
     * @param initialState of the context.
     * @returns {boolean} Whether the callback was successfully registered.
     */
    registerUndoRedoCallback = (ctx, callback, initialState) => {
        if (ctx in this._contexts) {
            return false;
        }
        this._contexts[ctx] = {
            undoStack: [],
            redoStack: [],
            callback: callback,
            initialState: JSON.stringify(initialState),
        };
        return true;
    };

    /**
     * @param ctx String describing the context.
     * @returns {boolean} Whether the callback was registered before deleting.
     */
    unregisterUndoRedoCallback = (ctx) => {
        if (!(ctx in this._contexts)) {
            return false;
        }
        delete this._contexts[ctx];
        return true;
    };

    /**
     * Does all count callbacks.
     * @private
     */
    _doCountCallbacks = () => {
        this._countCallbacks.forEach((callback) => {
            callback(this._undoContextStack.length, this._redoContextStack.length);
        });
    };

    /**
     * Does all undo/redo callbacks.
     * @param currCtx
     * @private
     */
    _doUndoRedoCallbacks = (currCtx) => {
        for (let ctx in this._contexts) {
            this._contexts[ctx].callback(this._getCurrentState(currCtx), currCtx);
        }
    };

    /**
     * Returns the current state.
     * @param ctx
     * @returns {null|*} Current state if there is one.
     * @private
     */
    _getCurrentState = (ctx) => {
        if (!(ctx in this._contexts)) {
            return null;
        }
        const undoStack = this._contexts[ctx].undoStack;
        if (undoStack.length === 0) {
            return JSON.parse(this._contexts[ctx].initialState);
        } else {
            return JSON.parse(undoStack[undoStack.length - 1]);
        }
    };

    /**
     * Enforces the max stack size and modifies the initial state if needed.
     * @private
     */
    _enforceMaxStackSize = () => {
        while (this._undoContextStack.length > MAX_STACK_SIZE) {
            const ctx = this._undoContextStack.shift();
            this._contexts[ctx].initialState = this._contexts[ctx].undoStack.shift();
        }
        while (this._redoContextStack.length > MAX_STACK_SIZE) {
            this._redoContextStack.shift();
        }
    };
}
