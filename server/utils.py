import os
import requests
import zipfile
from appdirs import user_cache_dir


class GoogleDriveDownloader:
    CHUNK_SIZE = 32768

    def __init__(self, uid, path, verbose=True):
        """
        Class for downloading a file from google drive.
        Adapted from https://stackoverflow.com/questions/38511444/python-download-files-from-google-drive-using-url
        :param uid: Str. ID from the google drive shareable link.
        :param path: Str. Path of the downloaded file.
        :param verbose: Bool.
        """
        self.uid = uid
        self.path = path
        self.verbose = verbose
    
    def download(self, unzip_file=False):
        """
        Downloads the specified file to the specified path.
        :param unzip_file: Bool. Whether to unzip the file and delete the original zip file.
        :return: Nothing.
        """
        if self.verbose:
            print('Downloading', self.path)
        URL = 'https://docs.google.com/uc?export=download'
        session = requests.Session()
        response = session.get(URL, params={'id': self.uid}, stream=True)
        token = GoogleDriveDownloader._get_confirm_token(response)
        if token:
            params = {'id': self.uid, 'confirm': token}
            response = session.get(URL, params=params, stream=True)
        self._save_response_content(response)
        if unzip_file:
            if self.verbose:
                print('Extracting', self.path)
            unzip(self.path, out_dir=os.path.dirname(self.path))
            os.remove(self.path)

    @staticmethod
    def _get_confirm_token(response):
        for key, value in response.cookies.items():
            if key.startswith('download_warning'):
                return value
        return None

    def _save_response_content(self, response):
        with open(self.path, 'wb') as file:
            for chunk in response.iter_content(GoogleDriveDownloader.CHUNK_SIZE):
                # filter out keep-alive new chunks
                if chunk:
                    file.write(chunk)


def unzip(path, out_dir=None):
    """
    :param path: Str. Path to the .zip file.
    :param out_dir: Optional str. Output directory.
    :return:  Nothing.
    """
    with zipfile.ZipFile(path) as zip_file:
        zip_file.extractall(path=out_dir)


def get_cache_dir():
    """
    :return: Str. Cache directory for this project.
    """
    return user_cache_dir('interpolator')


def get_saved_model_directories():
    """
    :return: Dict containing information about saved model directories.
    """
    dirs = {}
    dirs['base'] = os.path.join(get_cache_dir(), 'saved_models')
    dirs['flow'] = os.path.join(dirs['base'], 'flow_model')
    dirs['hinted_flow'] = os.path.join(dirs['base'], 'hinted_flow_model')
    dirs['synth'] = os.path.join(dirs['base'], 'synth_model')
    dirs['flow_frozen'] = os.path.join(dirs['flow'], 'frozen')
    dirs['hinted_flow_frozen'] = os.path.join(dirs['hinted_flow'], 'frozen')
    dirs['synth_frozen'] = os.path.join(dirs['synth'], 'frozen')
    return dirs
