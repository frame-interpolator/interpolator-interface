import argparse
import base64
import cv2
import http.server
import io
import json
import numpy as np
import os
import socketserver
import threading
import traceback
from PIL import Image, ImageCms
from utils import get_saved_model_directories


instance = {}


def _convert_to_srgb(img):
    """
    Convert PIL image to sRGB color space (if possible).
    :param img: PIL Image object.
    :return: Maybe converted image.
    """
    icc = img.info.get('icc_profile', '')
    if icc:
        io_handle = io.BytesIO(icc)  # virtual file
        src_profile = ImageCms.ImageCmsProfile(io_handle)
        dst_profile = ImageCms.createProfile('sRGB')
        img = ImageCms.profileToProfile(img, src_profile, dst_profile)
    return img


def decode_image(encoded):
    """
    Decodes an image that was sent from the javascript client.
    :param encoded: Str in web format.
    :return: Decoded image of shape [H, W, 3] and values between 0.0 and 1.0.
            alpha_mask has shape [H, W, 1]. Values are 0.0 for transparent areas and 1.0 for opaque areas.
    """
    base64_encoded_image = encoded.split('base64,', 1)[1]
    image = base64.b64decode(base64_encoded_image)
    stream = io.BytesIO(image)
    image_np = np.array(_convert_to_srgb(Image.open(stream)))
    if image_np.shape[2] == 4:
        alpha_mask = (image_np[:, :, 3:4] > 128).astype(np.float32)
        image_np = image_np[:, :, 0:3]
    else:
        alpha_mask = np.ones((image_np.shape[0], image_np.shape[1], 1), dtype=np.float32)
    image_np = image_np.astype(dtype=np.float32) / 255.0
    image_np = image_np * alpha_mask
    return image_np, alpha_mask


def encode_bgr_image(image):
    """
    :param image: BGR image of shape [H, W, 3] of type np.uint8.
    :return: Encoded string in web format.
    """
    return ('data:image/png;base64,' +
            base64.b64encode(cv2.imencode('.png', image)[1]).decode('utf-8'))


def encode_image(image):
    """
    Encodes an image to be sent back to the javascript client.
    :param image: Image of shape [H, W, 3] and values between 0.0 and 1.0.
    :return: Encoded string in web format.
    """
    image = image * 255.0
    image = image.astype(np.uint8)
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    return encode_bgr_image(image)


def encode_flow_visualizations(flow_0_1, flow_1_0):
    """
    :param flow_0_1: Flows of shape [H, W, 2].
    :param flow_1_0: Flows of shape [H, W, 2].
    :return: Encoded string in web format.
    """
    from common.utils.flow import get_flow_visualization
    vis_0_1 = cv2.cvtColor(get_flow_visualization(flow_0_1), cv2.COLOR_RGB2BGR)
    vis_1_0 = cv2.cvtColor(get_flow_visualization(flow_1_0), cv2.COLOR_RGB2BGR)
    return encode_bgr_image(vis_0_1), encode_bgr_image(vis_1_0)


def parse_hints(shape, hints):
    """
    Parses hints from the javascript client.
    :param shape: Tuple with 2 entries: (H, W).
    :param hints: Hint values from GUI.
    :return: Forward hints: Hints of shape [H, W, 2] and mask of shape [H, W, 1].
            Backward hints: Hints of shape [H, W, 2] and mask of shape [H, W, 1].
    """
    height, width = shape

    def in_bounds(pos):
        return 0 <= pos[0] < height and 0 <= pos[1] < width

    hint_0_1 = np.zeros(shape + (2,), dtype=np.float32)
    mask_0_1 = np.zeros(shape + (1,), dtype=np.float32)
    hint_1_0 = np.zeros(shape + (2,), dtype=np.float32)
    mask_1_0 = np.zeros(shape + (1,), dtype=np.float32)
    for hint in hints:
        if 'imageId' in hint:
            hint_switch = [hint_0_1, hint_1_0]
            mask_switch = [mask_0_1, mask_1_0]
            image_id = int(hint['imageId'])
            pos = [int(hint['y']), int(hint['x'])]
            if in_bounds(pos):
                hint_switch[image_id][pos[0], pos[1]] = [0.0, 0.0]
                mask_switch[image_id][pos[0], pos[1]] = [1.0]
        else:
            pos_0 = [int(hint['y']), int(hint['x'])]
            pos_1 = [int(hint['y'] + hint['dy']), int(hint['x'] + hint['dx'])]
            if in_bounds(pos_0):
                hint_0_1[pos_0[0], pos_0[1]] = [hint['dx'], hint['dy']]
                mask_0_1[pos_0[0], pos_0[1]] = [1.0]
            if in_bounds(pos_1):
                hint_1_0[pos_1[0], pos_1[1]] = [-hint['dx'], -hint['dy']]
                mask_1_0[pos_1[0], pos_1[1]] = [1.0]
    return hint_0_1, mask_0_1, hint_1_0, mask_1_0


def get_flow(image0_np, image1_np, hints, resolution_fraction):
    """
    :param image0_np: Image of shape [H, W, 3].
    :param image1_np: Image of shape [H, W, 3].
    :param hints: Array of hints from the front end.
    :param resolution_fraction: Float. The fraction to run the flow network at.
    :return: flow_0_1, flow_1_0
    """
    if hints is None:
        outputs = instance['runner'].get_flow(image0_np, image1_np, res_fraction=resolution_fraction)
    else:
        hints_0_1_np, hint_mask_0_1_np, hints_1_0_np, hint_mask_1_0_np = parse_hints(
            (image0_np.shape[0], image0_np.shape[1]), hints)
        outputs = instance['runner'].get_hinted_flow(image0_np, image1_np, hints_0_1_np, hint_mask_0_1_np,
                                                     hints_1_0_np, hint_mask_1_0_np,
                                                     res_fraction=resolution_fraction)
    return outputs


class InterpolatorRequestHandler(http.server.SimpleHTTPRequestHandler):
    def _set_headers(self, filename='/index.html'):
        if len(filename) >= 4 and filename[-4:] == '.css':
            type = 'text/css'
        elif len(filename) >= 3 and filename[-3:] == '.js':
            type = 'application/javascript'
        elif len(filename) >= 5 and filename[-5:] == '.json':
            type = 'application/javascript'
        elif len(filename) >= 4 and filename[-4:] == '.ico':
            type = 'image/x-icon'
        else:
            type = 'text/html'

        self.send_response(200)
        self.send_header('Content-type', type)
        self.end_headers()

    def do_GET(self):
        try:
            if self.path == '/':
                filename = os.path.join(instance['root'], 'index.html')
            else:
                # self.path is of form '/main.js', etc.
                filename = instance['root'] + self.path

            self._set_headers(filename)
            if filename.endswith('.ico'):
                with open(filename, 'rb') as file:
                    self.wfile.write(file.read())
            else:
                with open(filename, 'r', encoding='utf-8') as fh:
                    self.wfile.write(fh.read().encode('utf-8'))
        except Exception as error:
            print(error)
            print('Exception encountered.')

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        print(self.headers)
        length = self.headers.get('content-length')
        length = int(length) if length is not None else 0
        read = self.rfile.read(length).decode('utf-8')
        body_dict = json.loads(read)
        print('Got request:', body_dict.keys())
        image0_np, image0_alpha_mask = decode_image(body_dict['img0'])
        image1_np, image1_alpha_mask = decode_image(body_dict['img1'])
        times = body_dict.get('times', [0.5])
        hints = body_dict.get('hints', None)
        get_middlebury_interp = body_dict.get('get_middlebury_interp', False)
        get_bidir_interp = body_dict.get('get_bidir_interp', False)
        get_flow_visualizations = body_dict.get('get_flow_visualizations', False)
        resolution_fraction = body_dict.get('resolution_fraction', 1.0)
        quadratic_motion = body_dict.get('quadratic_motion', False)
        response = {}
        try:
            # Get flow and visualizations.
            outputs = get_flow(image0_np, image1_np, hints, resolution_fraction)
            flow_0_1, flow_1_0 = outputs
            if get_flow_visualizations:
                flow_vis_0_1, flow_vis_1_0 = encode_flow_visualizations(flow_0_1, flow_1_0)
                response['flow_visualizations'] = [flow_vis_0_1, flow_vis_1_0]
            # Apply alpha to the flow.
            flow_0_1, flow_1_0 = instance['runner'].apply_alpha_mask(flow_0_1, flow_1_0,
                                                                     image0_alpha_mask, image1_alpha_mask)
            # Get flow midpoints.
            flow_0_1_midpoints = None
            flow_1_0_midpoints = None
            if quadratic_motion:
                flow_0_1_midpoints = instance['runner'].get_quadratic_motion_midpoints(flow_0_1)
                flow_1_0_midpoints = instance['runner'].get_quadratic_motion_midpoints(flow_1_0)
            # Do interpolations.
            images_reencoded = []
            for t in times:
                interped = instance['runner'].interp(image0_np, image1_np, flow_0_1, flow_1_0, t,
                                                     flow_0_1_midpoints=flow_0_1_midpoints,
                                                     flow_1_0_midpoints=flow_1_0_midpoints)
                images_reencoded.append(encode_image(interped))
            response['images'] = images_reencoded
            # Get interpolation extras.
            if get_middlebury_interp:
                foward_interped_middlebury, backward_interped_middlebury = instance['runner'].get_middlebury_interp(
                    image0_np, image1_np, flow_0_1, flow_1_0, 0.5, flow_0_1_midpoints, flow_1_0_midpoints)
                response['foward_interped_middlebury'] = encode_image(foward_interped_middlebury)
                response['backward_interped_middlebury'] = encode_image(backward_interped_middlebury)
            if get_bidir_interp:
                forward_interped_bidir, backward_interped_bidir = instance['runner'].get_bidir_interp(
                    image0_np, image1_np, flow_0_1, flow_1_0, 0.5, flow_0_1_midpoints, flow_1_0_midpoints)
                response['forward_interped_bidir'] = encode_image(forward_interped_bidir)
                response['backward_interped_bidir'] = encode_image(backward_interped_bidir)
            # Get midpoint visualizations.
            if get_flow_visualizations and quadratic_motion:
                midpoint_vis_0_1, midpoint_vis_1_0 = encode_flow_visualizations(flow_0_1_midpoints, flow_1_0_midpoints)
                response['forward_flow_midpoints'] = [midpoint_vis_0_1, midpoint_vis_1_0]
            # Create response.
            response_json = json.dumps(response)
            self.send_response(200)
            self.send_header('Content-type', 'application/x-www-form-urlencoded')
            self.end_headers()
            self.wfile.write(response_json.encode('utf-8')) 
        except Exception as error:
            traceback.print_exc()
            print(error)
            print('Exception encountered.')


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


def loop_until_eof():
    """
    Runs a loop until an end of file character is received.
    """
    try:
        line = input()
        while line is not None:
            line = input()
    except EOFError as _:
        return


def run(interp_model, flow_model, hinted_flow_model, root, port=80, interp_dir=os.curdir):
    # Set up the server.
    server_address = ('', port)
    server = ThreadedTCPServer(server_address, InterpolatorRequestHandler)
    print('Starting server on port %d...' % port)
    server_thread = threading.Thread(target=server.serve_forever)
    instance['root'] = os.path.abspath(root)
    # Import and start the interpolator.
    os.chdir(interp_dir)
    from common.utils.interp_eval import InterpRunner
    print('Loading models from:', interp_model, flow_model, hinted_flow_model)
    instance['runner'] = InterpRunner(interp_model, flow_model, hinted_flow_model)
    # Exit the server thread when the main thread terminates.
    server_thread.daemon = True
    server_thread.start()
    loop_until_eof()
    print('Shutting down server...')
    server.shutdown()
    server.server_close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='A simple server.')
    parser.add_argument('--port', '-p', type=int, default=4399,
            help='Server port.')
    parser.add_argument('--root', '-r', type=str, default=os.curdir,
            help='Server root (directory containing index.html)')
    parser.add_argument('--interpolator', '-i', type=str,
            help='Directory of the interpolator repository.')
    # Saved model paths.
    parser.add_argument('--interp_model', '-im', type=str, default=None,
            help='Directory of the synthesizer model. If not provided, the downloaded model will be used.')
    parser.add_argument('--flow_model', '-fm', type=str, default=None,
            help='Directory of the flow model. If not provided, the downloaded model will be used.')
    parser.add_argument('--hinted_flow_model', '-hfm', type=str, default=None,
                        help='Directory of the hinted flow model. If not provided, the downloaded model will be used.')
    args = parser.parse_args()
    dirs = get_saved_model_directories()
    if args.interp_model is None:
        args.interp_model = dirs['synth_frozen']
    if args.flow_model is None:
        args.flow_model = dirs['flow_frozen']
    if args.hinted_flow_model is None:
        args.hinted_flow_model = dirs['hinted_flow_frozen']
    run(args.interp_model, args.flow_model, args.hinted_flow_model,
        root=args.root, port=args.port, interp_dir=args.interpolator)
