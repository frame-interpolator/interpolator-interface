import argparse
import base64
import http.server
import json
import os
import socketserver
import threading
import urllib.request


Handler = http.server.SimpleHTTPRequestHandler


instance = {}


class RequestHandler(Handler):
    def _set_headers(self):
        self.send_response(200)
        self.end_headers()

    def do_GET(self):
        file = urllib.request.urlopen(instance['target'] + self.path)
        self._set_headers()
        self.copyfile(file, self.wfile)

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        length = self.headers.get('content-length')
        length = int(length) if length is not None else 0
        data = self.rfile.read(length)
        req = urllib.request.Request(instance['target'] + self.path, data)
        self._set_headers()
        file = urllib.request.urlopen(req)
        self.copyfile(file, self.wfile)


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


def loop_until_eof():
    """
    Runs a loop until an end of file character is received.
    """
    try:
        line = input()
        while line is not None:
            line = input()
    except EOFError as _:
        return


def run(target, port=80):
    instance['target'] = target
    server_address = ('', port)
    server = ThreadedTCPServer(server_address, RequestHandler)
    print('Starting server on port %d...' % port)
    server_thread = threading.Thread(target=server.serve_forever)
    # Exit the server thread when the main thread terminates.
    server_thread.daemon = True
    server_thread.start()
    loop_until_eof()
    print('Shutting down server...')
    server.shutdown()
    server.server_close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Proxy server.')
    parser.add_argument('--port', '-p', type=int, default=4950,
            help='Proxy server port.')
    parser.add_argument('--target', '-t', type=str,
            help='Target server host name and port (i.e. http://www.example.com:80.')
    args = parser.parse_args()
    run(args.target, port=args.port)
    
