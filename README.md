[![alt text is for people with bad internet](https://gitlab.com/frame-interpolator/interpolator-interface/raw/master/docs/demo.png)](https://gitlab.com/frame-interpolator/interpolator-interface/raw/master/docs/demo.png)

# Interpolator Interface

#### [Official Wiki](https://gitlab.com/frame-interpolator/interpolator-interface/wikis/home)

# Quick links
* [Installation](https://gitlab.com/frame-interpolator/interpolator-interface/wikis/Installation)
* [Run with a server](https://gitlab.com/frame-interpolator/interpolator-interface/wikis/Run)
